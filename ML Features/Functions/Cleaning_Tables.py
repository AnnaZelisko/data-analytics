# Databricks notebook source
#imports
import pandas as pd
import numpy as np

# COMMAND ----------

def flagging_unhelpful_columns(df):
    """
    Highlights columns that dont have any information or are web based developer columns.
    
    Parameters
    -------
    df: pandas 
        Original table with all our columns.
    
    Returns
    -------
    Dictionary 
        Columns with reasons as to why the columns were unhelpful.
    """
    dic_of_cols = {}
    
    for col in df.columns:
        
        if len(df[col])==0:
            dic_of_cols[col] = "Empty Column"
            
        if df[col].nunique() <= 1:
            dic_of_cols[col] = "One Value"
       
        if (df[col].isnull().sum()/ len(df)) >= 0.75:
            dic_of_cols[col] = "75% empty"
            
        if "Error" in col:
            dic_of_cols[col] = "Website Error"
        
        if "Formula" in col:
            dic_of_cols[col] = "Formula"
        
        if "Output" in col:
            dic_of_cols[col] = "Outputting to screen"
    
    return dic_of_cols

# COMMAND ----------

def low_varience_columns(df):
    """
    Columns with low variance or is dominated by one value.
    
    Parameters
    -------
    df: pandas 
        Original table with all our columns.
    
    Returns
    -------
    List 
        Columns with less than 80% varience.
    """
    cols = []
    
    for col in df.columns:
        if (df.groupby([col]).count()/len(df)).max()[0] >= 0.80:
            cols.append(col)
    
    return cols

# COMMAND ----------

def flagging_id_columns(df):
    """ 
    Flags columns that are fact ids.
    
    Parameters
    -------
    df: pandas 
        Original table with all our columns.
    
    Returns
    -------
    List 
        Columns that hold fact ids
    """
    import re
    
    cols = []
    
    for col in df.columns:
        val = df[col].dropna().unique()
        if len(val) == 0:
            continue
        elif isinstance(val[0], str) and len(val[0]) >= 15 and bool(re.search(r'\d', val[0])):
            cols.append(col)
    
    return cols

# COMMAND ----------

def flagging_columns_high_correlation(df, target_col):
    """ 
    Flags columns that have a high correlation.
    Then decide between the two columns that have a high correlation which one to drop.
    This decision depends on who has a better correlation with the target.
    
    Parameters
    -------
    df: pandas 
        Original table with all our columns.
    target_col:str
        Str that is the main column for our correlation table.
    
    Returns
    -------
    List 
        Columns that have a high correlation.
    """
    correlation_df = df.corr().abs()
    
    stack = correlation_df.unstack()
    sorted_stack = stack.sort_values(kind="quicksort")
    
    correlation_df = sorted_stack.to_frame().reset_index()
    correlation_df.columns = ['Column 1', 'Column 2', 'Correlation']
    correlation_df = correlation_df[(correlation_df['Correlation']>= 0.8) & (correlation_df['Column 1']!=correlation_df['Column 2'])]
    correlation_df = correlation_df.iloc[::2]
    
    correlation_columns_for_dropping = []
    for index, row in correlation_df.iterrows():
        col_1 = row['Column 1']
        col_2 = row['Column 2']
        corr_with_col1 = df[target_col].corr(df[col_1])
        corr_with_col2 = df[target_col].corr(df[col_2])

        if corr_with_col1 >= corr_with_col2:
            correlation_columns_for_dropping.append(col_2)
        else:
            correlation_columns_for_dropping.append(col_1)
    
    return correlation_columns_for_dropping
