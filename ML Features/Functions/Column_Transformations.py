# Databricks notebook source
import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = ''
for x in range(0,dir_to_data):
    string += '..\\'
    
sys.path.insert(0,string)

# COMMAND ----------

#imports
import pandas as pd
import numpy as np

from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL, GET_from_Excel

import Configuration_File

# COMMAND ----------

def converting_currency_to_CAD(df, monotery_value_column, currency_type_column):
    """
    Converts column values from international currency to canadian currencies based on rates in Salesforce.
    
    Parameters
    -------
    df: pandas 
        Original dataframe which has monotery columns.
    monotery_value_column: list 
        Column(s) that need to be converted from one.
    currency_type_column: str
        Column with the currency that the monotery value needs to be converted from
    
    
    Returns
    -------
    pandas 
        Dataframe of original table with converted column and rate column
    """
    cols_for_Currency_Type = ['CurrencyType Currency ISO Code','CurrencyType Conversion Rate']
    Currency_Type_Info = GET_from_Centra_SQL(Configuration_File.CURRENCYTYPE, cols_for_Currency_Type)

    df_with_currency_rate = pd.merge(df, Currency_Type_Info, how='left', 
                                     left_on=currency_type_column, 
                                     right_on='CurrencyType Currency ISO Code')\
                              .drop(['CurrencyType Currency ISO Code'],1)
    
    for col in monotery_value_column:
        df_with_currency_rate[col+' (CAD)'] = (df_with_currency_rate[col]
                                               *(1/df_with_currency_rate['CurrencyType Conversion Rate'])).round(2)
    
    return df_with_currency_rate

# COMMAND ----------

def MERGE_Features_to_Base(df, key_word, join_column):
    """
    Merges the feature tables from config to the base table.
    
    Parameters
    -------
    df: pandas
        Dataframe of the table we wil be merging to.
    key_word: str 
        Key is the word prefacing all the tables with the releveant features.
        For example all quote line features will have the varialbe name QUOTELINES before them.
    join_column: str
        Key join column before all the tables.
        
    Returns
    -------
    pandas 
        Dataframe with all features associated to key word.
    """
    for variable, value in vars(Configuration_File).items():
        if key_word in variable and "EXCEL_SHEET" not in variable:
            col_name = variable.replace("_", " ").title()
            Features_Table = GET_from_Excel(value, 0)
            Features_Table.columns = [join_column if col ==join_column else col_name+" "+col 
                                       for col in Features_Table.columns]

            df = pd.merge(df, Features_Table, how="left", on=join_column)
        
    return df

# COMMAND ----------

def transforming_tf_to_binary_columns(df):
    """
    Columns that are fact ids.
    
    Parameters
    -------
    df: pandas 
        Dataframe which has all our columns.
    
    Returns
    -------
    pandas
        Dataframe with True and False columns set to binary.
    """

    for col in df.columns:
        values_ls = df[col].unique()
        if True in values_ls or "True" in values_ls:
            df[col] = df[col].astype(float)
    
    return df

# COMMAND ----------

def engineering_date_features(df):
    """
    Turning a date column into multiple numerical date features. 
    
    Parameters
    -------
    df: pandas 
        Dataframe which has all our columns.
    
    Returns
    -------
    pandas
        Dataframe with new date features.
    """  
    import datetime as dt
    
    for col in df.columns:
        if df[col].dtype == "datetime64[ns]":
            df['Week in Month of '+col] = df[col].apply(lambda d: (d.day-1) // 7 + 1)
            df['Month of Quote '+col] = df[col].dt.month
            df['Day of Quote '+col] = df[col].dt.day
            df['Year of '+col] = df[col].dt.year
            df['Season of '+col] = (df[col].dt.month%12 + 3)//3
            df[col] = df[col].map(dt.datetime.toordinal)
            
    return df

# COMMAND ----------

def encoding_string_with_weight_of_evidence(df, target_col_str, threshold):
    """
    Columns that are fact ids.
    
    Parameters
    -------
    df: pandas 
        Dataframe which has all our columns.
    target_col_str: str
        Target Column for prediction.
    threshold: int
        Number of unique values in column.
    
    Returns
    -------
    pandas
        Dataframe with some variables
    """
    encoder_mapping_dic = {}

    for col in df.columns:
        if df[col].dtype == object:
            prob_df = df.groupby(col)[target_col_str].mean().reset_index()
            prob_df['ratio'] = np.log(prob_df[target_col_str] /(1-prob_df[target_col_str]))
            prob_df.index = prob_df[col]

            ordinal_mapping =  prob_df['ratio'].to_dict()

            encoder_mapping_dic[col] = ordinal_mapping
            exploratory_ana[col] = exploratory_ana[col].map(ordinal_mapping)
    
    return df, encoder_mapping_dic

# COMMAND ----------

def encoding_string_with_mean_encoding(df, target_col_str, threshold):
    """
    Encodes 
    
    Parameters
    -------
    df: pandas 
        Dataframe which has all our columns.
    target_col_str: str
        Target Column for prediction.
    threshold: int
        Number of unique values in column.
    
    returns dataframe with True and False columns set to bianary
    """
    encoder_mapping_dic = {}

    for col in df.columns:
        if df[col].dtype == object:
            Mean_encoded_subject = df.groupby([col])[target_col_str].mean().to_dict() 
            print(col) 
            print(Mean_encoded_subject) 
            unencoder[col] = Mean_encoded_subject
            df[col] =  df[col].map(Mean_encoded_subject) 
    
    return df, encoder_mapping_dic
