# Databricks notebook source
import pyodbc
import pandas as pd

# COMMAND ----------

Connection_to_DB = pyodbc.connect('Driver={SQL Server};'
                      'Server=rexsqlp04;'
                      'Database=CENTRA;'
                      'Trusted_Connection=yes;')

# COMMAND ----------

def GET_from_Centra_SQL(table_name, columns=[], where_clause=""):
    """
    Get a table from our Centra SQL data base.
    
    Parameters
    -------
    table_name: str
        Name of table to be retrieved from Centra SQL database.
    columns: list 
        Columns to be retrieved from the table, if list remains empty all columns will be retrieved 
    where_clause: str 
        Any additional where clauses for table
    
    Returns
    -------
    pandas 
        Dataframe of table.
    """
    sql_statement = 'SELECT '
    if not columns:
        #list empty
        sql_statement = sql_statement + '* FROM '
    else:
        sql_statement = sql_statement + '"'+'", "'.join(columns)+'" FROM '
        
        
    sql_statement = sql_statement + table_name + where_clause
    
    Table_Info = pd.read_sql(sql_statement, Connection_to_DB)
    
    return Table_Info

# COMMAND ----------

def GET_from_Excel(excel_link, excel_file_type, params = {}):
    """
    Get a table from a local excel.
    
    Parameters
    -------
    excel_link: str
        Link to excel file.
    excel_file_type: str
        Excels have many versions. Parameter allows us to specify which format type we are reading from.
        Currently supporting .xlsx and .csv
    params: dictionary
        Any additional parameters that a user wants to include.
    
    Returns
    -------
    pandas 
        Dataframe of excel.
    """
    if excel_file_type == "xlsx":
        return pd.read_excel(excel_link, **params)
    
    else: return pd.read_csv(excel_link, **params)
        
    

# COMMAND ----------


