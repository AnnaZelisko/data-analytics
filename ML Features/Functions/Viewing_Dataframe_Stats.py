# Databricks notebook source
import pandas as pd

# COMMAND ----------

def viewing_overall_df_stats(df, pred=None):
    """
    Get a table from our Centra SQL data base.
    
    Parameters
    -------
    df: pandas
        Original dataframe for which we would like statistics.
    pred: str 
        Column which will be predicted on.
    
    Returns
    -------
    pandas 
        Sumamry statistics of table.
    """ 
    obs = df.shape[0]
    types = df.dtypes
    counts = df.apply(lambda x: x.count())
    uniques = df.apply(lambda x: [x.unique()])
    nulls = df.apply(lambda x: x.isnull().sum())
    distincts = df.apply(lambda x: x.unique().shape[0])
    missing_ration = (df.isnull().sum()/ obs) * 100
    skewness = df.skew()
    kurtosis = df.kurt() 
    print('Data shape:', df.shape)
    
    if pred is None:
        cols = ['types', 'counts', 'distincts', 'nulls', 'missing ration', 'uniques', 'skewness', 'kurtosis']
        str = pd.concat([types, counts, distincts, nulls, missing_ration, uniques, skewness, kurtosis], axis = 1)

    else:
        corr = df.corr()[pred]
        stats_df = pd.concat([types, counts, distincts, nulls, missing_ration, uniques, skewness, kurtosis, corr], 
                             axis = 1, sort=False)
        corr_col = 'corr '  + pred
        cols = ['types', 'counts', 'distincts', 'nulls', 'missing_ration', 'uniques', 'skewness', 'kurtosis', corr_col ]
    
    stats_df.columns = cols
    dtypes = stats_df.types.value_counts()
    print('___________________________\nData types:\n',stats_df.types.value_counts())
    print('___________________________')
    return stats_df


# COMMAND ----------


