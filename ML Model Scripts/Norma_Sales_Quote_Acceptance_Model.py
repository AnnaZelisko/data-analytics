# Databricks notebook source
# MAGIC %md
# MAGIC # Norma - Sales Quote Acceptance Model
# MAGIC ### Mapping string, feature selection, general input table for the sales quote acceptance model.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Encoding_Strings">Encoding Strings</a><br>
# MAGIC <a href="#Scaling_Features">Scaling Features</a><br>
# MAGIC <a href="#Feature_Selection_Filter">Feature Selection Using Filtering Methods</a><br>
# MAGIC <a href="#Feature_Selection_Wrapper">Feature Selection Using Wrapper Methods</a><br>
# MAGIC <a href="#Cross_Comparing_Models">Cross Comparing Models</a><br>
# MAGIC <a href="#Determining_Model_Parameters">Determining Model Parameters</a><br>
# MAGIC <a href="#Fitting_Final_Model">Fitting Final Model</a><br>
# MAGIC <a href="#Feature_Importance">Feature Importance</a><br>
# MAGIC <a href="#Final_Model_Predictions">Final Model Predictions</a><br>
# MAGIC <a href="#Saving_Model_and_Parameterse">Saving Final Model and Parameters</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = "..\\" * dir_to_data
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

# general python imports
import numpy as np
import pandas as pd
import pickle

# visualizations
import matplotlib.pyplot as plt
import seaborn as sns

# machine learning imports
from sklearn.feature_selection import mutual_info_classif, VarianceThreshold, RFECV, SelectFromModel
from sklearn.preprocessing import StandardScaler
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.model_selection import KFold, StratifiedKFold, train_test_split
from sklearn import model_selection
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import classification_report

# custom function imports 
from ipynb.fs.full.Getting_Data import GET_from_Excel
from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns
from ipynb.fs.full.Viewing_Dataframe_Stats import viewing_overall_df_stats

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Getting the base features for this model. Looking at the basic overview of the statistics.

# COMMAND ----------

base_feature_df = GET_from_Excel(Configuration_File.SALES_QUOTE_ACCPTENCE_FEATURE_BASE, "csv")\
                        .drop(['Quote Line Predicted Revenue (CAD)_Sum', 'Days Quote Created to Close'],1)

# COMMAND ----------

details = viewing_overall_df_stats(base_feature_df, 'Target')
display(details.sort_values(by='corr Target', ascending=False))

# COMMAND ----------

X = base_feature_df.drop(['Target'],1)
y = base_feature_df['Target']

# COMMAND ----------

# split base features into train, validation and test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=1)
#cv_split

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Encoding_Strings'></a>
# MAGIC 
# MAGIC ## Encoding Strings
# MAGIC ### Getting the base features for this model. Looking at the basic overview of the statistics.
# MAGIC ### Label encoding isnt a good fit as we lack order in our categorical variables, one hot encoding will throw us into the risk for the dimentionality curse, target encoding will have a high risk of overfitting. As a result our best bet would be trying mean encoding accross k folds.

# COMMAND ----------

cat_col_ls = ["Quote Bill To City", "Quote Bill To Country", "Quote Bill To State", "Quote Owner Title", 
              "Quote Customer Country", "Quote Customer GBU", "Quote Created By Sales Office", 
              "Quote Created By Department", "Quote Account Billing Country", "Quote Region Country", "Quote Region City", 
              "Quote Primary Service Delivery Location Country", "Quote Primary Service Delivery Location City"]

# COMMAND ----------

kf = KFold(n_splits=350)
kf.get_n_splits(X_train)

# COMMAND ----------

mapping_cat_dic = {}
for train_index, group_to_ignore_index in kf.split(X_train, y_train):
    temp_df = pd.merge(X_train.iloc[train_index], y_train.iloc[train_index], left_index=True, right_index=True)
    for col in cat_col_ls:
        mean_mapping = temp_df.groupby(col)['Target'].mean().to_dict()
        if col in mapping_cat_dic:
            current_dic_map = mapping_cat_dic[col]
            mapping_cat_dic[col] = {k: (current_dic_map.get(k, 0) + mean_mapping.get(k, 0)) 
                                    for k in set(current_dic_map) | set(mean_mapping)}
        else:
            mapping_cat_dic[col] = mean_mapping

# COMMAND ----------

for col_key,dictionary_value in mapping_cat_dic.items():
    for key,value in dictionary_value.items():
        dictionary_value[key] = value/350

# COMMAND ----------

mapping_cat_dic

# COMMAND ----------

X_train.info()

# COMMAND ----------

X_val['Quote Customer GBU'].unique()

# COMMAND ----------

X_val['Quote Customer GBU'].dtype

# COMMAND ----------

for key,value in mapping_cat_dic.items():
    print(key)
    X_train[key] = X_train[key].replace(value)
    
    X_val[key] = X_val[key].replace(value)
    if X_val[key].dtype != np.number:
        X_val[key] = X_val[key].replace(regex={r'\D+':0})
    
    X_test[key] = X_test[key].replace(value)
    if X_test[key].dtype != np.number:
        X_test[key] = X_test[key].replace(regex={r'\D+':0})

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Scaling_Features'></a>
# MAGIC 
# MAGIC ## Scaling Features 
# MAGIC ### As I will be running a classification models most models benefit from feature scaling. As a result we will scale any features using the standard scaler approach: x-mean/std

# COMMAND ----------

# Standard Scaler
scaler_obj = StandardScaler()

# fit on training
scaler_obj.fit(X_train)

# transform data sets
scaler_obj.transform(X_train)
scaler_obj.transform(X_val)
scaler_obj.transform(X_test)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Feature_Selection_Filter'></a>
# MAGIC 
# MAGIC ## Data Feature Selection - Filter Methods
# MAGIC ### Below we will use the filter methods: varience, correlation maps, information gain and fisher score to filter down our feature set. Currently our feature table ahs over 300 features. Not all of these will be helpful to our model.

# COMMAND ----------

# getting rid of unhelpful columns - removing constant features
unhelpful_col_dic = flagging_unhelpful_columns(X_train)

# COMMAND ----------

# removing quasi-constant features, features dominanted by one value
quasi_constant_features_obj = VarianceThreshold(threshold=0.01)
quasi_constant_features_obj.fit(X_train)

quasi_constant_ls = [ x for x in X_train.columns if x not in X_train.columns[quasi_constant_features_obj.get_support()]]

# COMMAND ----------



# COMMAND ----------

quasi_constant_ls

# COMMAND ----------

X_train = X_train.drop(list(unhelpful_col_dic.keys())+quasi_constant_ls,1)

# COMMAND ----------

# correlation
# Sample figsize in inches
fig, ax = plt.subplots(figsize=(20,10))         
# Imbalanced DataFrame Correlation
corr = X_train.corr()
sns.heatmap(corr, cmap='YlGnBu', annot_kws={'size':30}, ax=ax)
ax.set_title("Correlation Matrix", fontsize=14)
plt.show()

# COMMAND ----------

c = X_train.corr().abs()

s = c.unstack()
so = s.sort_values(kind="quicksort")

# COMMAND ----------

correlation_df = so.to_frame().reset_index()
correlation_df.columns = ['Column 1', 'Column 2', 'Correlation']
correlation_df = correlation_df[(correlation_df['Correlation']>= 0.8) 
                                & (correlation_df['Column 1']!=correlation_df['Column 2'])]
correlation_df = correlation_df.iloc[::2]
correlation_df

# COMMAND ----------

X_train = X_train.drop(list(correlation_df['Column 1'].unique()),1)

# COMMAND ----------

# information gain
importance = mutual_info_classif(X_train, y_train)
feature_information_gain_dic = pd.Series(importance, X_train.columns)

# COMMAND ----------

feature_with_no_importance = feature_information_gain_dic[feature_information_gain_dic==0]

# COMMAND ----------

X_train = X_train.drop(list(feature_with_no_importance.keys()), 1)

# COMMAND ----------

# i=0
# while i < X_train.shape[1]:
#     if variance_inflation_factor(X_train.values, i) > 10:
#         print(i)
#         print(X_train.columns[i])
#         print(variance_inflation_factor(X_train.values, i))
#         X_train = X_train.drop(X_train.columns[i],1)
#     i+=1

# COMMAND ----------

variables = range(X.shape[1])
dropped=True
while dropped:
    dropped=False
    vif = [variance_inflation_factor(X_train.values, ix) for ix in range(X_train.shape[1])]

    maxloc = vif.index(max(vif))
    print(max(vif))
    if max(vif) > 10:
        print('dropping \'' + X_train.columns[maxloc] + '\' at index: ' + str(maxloc))
        X_train = X_train.drop(X_train.columns[maxloc],1)
        dropped=True

# COMMAND ----------

X_train.shape[1]

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Feature_Selection_Wrapper'></a>
# MAGIC 
# MAGIC ## Wrapper Feature Selection - Recursive Feature Selection
# MAGIC ### As we are building a classification model, I will recursivly select 

# COMMAND ----------

rfecv = RFECV(estimator=DecisionTreeClassifier(), step=10, cv=StratifiedKFold(10), scoring='accuracy')
rfecv.fit(X_train, y_train)

# COMMAND ----------

print('Optimal number of features: {}'.format(rfecv.n_features_))

# COMMAND ----------

print(np.where(rfecv.support_ == False)[0])

# COMMAND ----------

index_for_selected_col_ls = np.where(rfecv.support_ == True)[0]

# COMMAND ----------

index_for_selected_col_ls

# COMMAND ----------

rfecv_col_selected_ls = [X_train.columns[x] for x in range(len(X_train.columns)) if x in index_for_selected_col_ls]       

# COMMAND ----------

len(rfecv_col_selected_ls)

# COMMAND ----------

X_train = X_train[rfecv_col_selected_ls]

# COMMAND ----------

X_train

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Cross_Comparing_Models'></a>
# MAGIC 
# MAGIC ## Cross Comparing Models 
# MAGIC 
# MAGIC ### Running a basic comparision of classification models. To guage which model will out perform the others based on just the feature selection list.

# COMMAND ----------

selected_columns_model_ls = list(X_train.columns)
X_test = X_test[selected_columns_model_ls]

# COMMAND ----------

models_ls = [("LogReg", LogisticRegression()), 
              ("RF", RandomForestClassifier()),
              ("DesTree", DecisionTreeClassifier()),
              ("GNB", GaussianNB())]

#took long and didnt give best results
# ("KNN", KNeighborsClassifier()),
# ("SVM", SVC()), 

results = []
names = []
scoring_ls = ['accuracy', 'precision_weighted', 'recall_weighted', 'f1_weighted', 'roc_auc']
final_scores_df_ls = []

for name, model in models_ls:
    kfold = model_selection.KFold(n_splits=10, shuffle=True, random_state=90210)
    cv_results = model_selection.cross_validate(model, X_train, y_train, cv=kfold, scoring=scoring_ls)
    clf = model.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    print(name)
    print(classification_report(y_test, y_pred))
    results.append(cv_results)
    names.append(name)
    this_df = pd.DataFrame(cv_results)
    this_df['model'] = name
    final_scores_df_ls.append(this_df)

final = pd.concat(final_scores_df_ls, ignore_index=True)

# COMMAND ----------

final

# COMMAND ----------

final

# COMMAND ----------

final_model = RandomForestClassifier()

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Determining_Model_Parameters'></a>
# MAGIC 
# MAGIC ## Selecting Model Parameters
# MAGIC ### Now that we have a base model we can grid search for model parameters that can improve our model. Im using the validation set to determine parameter set.

# COMMAND ----------

skf = StratifiedKFold(n_splits=10, shuffle=True)

# COMMAND ----------

from sklearn.model_selection import GridSearchCV
params = {
    'n_estimators': [10, 50, 100, 150, 200],
    'criterion': ["gini", "entropy"],
    'max_depth': [1,5,10, None]
}

grid_no_up = GridSearchCV(final_model, param_grid=params, cv=skf, 
                          scoring=scoring_ls, refit='accuracy', n_jobs=-1).fit(X_val, y_val)


# COMMAND ----------

# best suggested parameters based on grid search
grid_no_up.best_params_

# COMMAND ----------

# final models score should beat this one
grid_no_up.best_score_

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Fitting_Final_Model'></a>
# MAGIC 
# MAGIC ## Fitting Final Model
# MAGIC ### Setting up and fitting our final model with our best parameters.

# COMMAND ----------

final_model = RandomForestClassifier(**grid_no_up.best_params_)

# COMMAND ----------

# fit the model
final_model.fit(X_train, y_train)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Feature_Importance'></a>
# MAGIC 
# MAGIC ## Feature Importance
# MAGIC ### Seeing the weights associated to each of the features will allow me to see what influences the model the most.

# COMMAND ----------

# get importance
importance = final_model.feature_importances_
# summarize feature importance
for i,v in enumerate(importance):
    print('Feature: %0s, Index: %.5f, Score: %.5f' % (X_train.columns[i], i,v))
# plot feature importance
plt.bar([x for x in range(len(importance))], importance)
plt.show()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Final_Model_Predictions'></a>
# MAGIC 
# MAGIC ## Predicting Final Model On Test Data
# MAGIC ### Checking prediction of final model on test data.

# COMMAND ----------

y_test_pred = final_model.predict(X_test)
print('Accuracy of model on test set: {:.2f}'.format(final_model.score(X_test, y_test)))

# COMMAND ----------

from sklearn.metrics import confusion_matrix
confusion_matrix = confusion_matrix(y_test, y_test_pred)
print(confusion_matrix)

# COMMAND ----------

from sklearn.metrics import classification_report
print(classification_report(y_test, y_test_pred))

# COMMAND ----------

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
logit_roc_auc = roc_auc_score(y_test, final_model.predict(X_test))
fpr, tpr, thresholds = roc_curve(y_test, final_model.predict_proba(X_test)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Model (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('Log_ROC')
plt.show()

# COMMAND ----------

X_val = X_val[selected_columns_model_ls]

# COMMAND ----------

y_val_pred = final_model.predict(X_val)
print('Accuracy of model on test set: {:.2f}'.format(final_model.score(X_val, y_val)))

# COMMAND ----------

from sklearn.metrics import confusion_matrix
confusion_matrix = confusion_matrix(y_val, y_val_pred)
print(confusion_matrix)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Saving_Model_and_Parameters'></a>
# MAGIC 
# MAGIC ## Saving Model
# MAGIC ### Saving our  model so it can be re run

# COMMAND ----------

# mapping of categorical variables, scaler then selected column and final trained model
all_model_info = [mapping_cat_dic, scaler_obj, selected_columns_model_ls, final_model]

# COMMAND ----------

# save the model to disk
pickle.dump(all_model_info, open(Configuration_File.SALES_QUOTE_ACCPTENCE_MODEL, 'wb'))

# COMMAND ----------


