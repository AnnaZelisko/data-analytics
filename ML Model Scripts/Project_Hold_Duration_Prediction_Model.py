# Databricks notebook source
# MAGIC %md
# MAGIC # Project Hold Duration Prediction Model
# MAGIC ### Originally a part of the Master Scheduler project, the goal of this model is to predict holds and their duration. This will be a multi-output model as we break the hold down by 10 bins within project bin.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Encoding_Strings">Encoding Strings</a><br>
# MAGIC <a href="#Scaling_Features">Scaling Features</a><br>
# MAGIC <a href="#Feature_Selection_Filter">Feature Selection Using Filtering Methods</a><br>
# MAGIC <a href="#Feature_Selection_Wrapper">Feature Selection Using Wrapper Methods</a><br>
# MAGIC <a href="#Cross_Comparing_Models">Cross Comparing Models</a><br>
# MAGIC <a href="#Determining_Model_Parameters">Determining Model Parameters</a><br>
# MAGIC <a href="#Fitting_Final_Model">Fitting Final Model</a><br>
# MAGIC <a href="#Feature_Importance">Feature Importance</a><br>
# MAGIC <a href="#Final_Model_Predictions">Final Model Predictions</a><br>
# MAGIC <a href="#Saving_Model_and_Parameterse">Saving Final Model and Parameters</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = "..\\" * dir_to_data
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

# general python imports
import numpy as np
import pandas as pd
import pickle

# visualizations
import matplotlib.pyplot as plt
import seaborn as sns

# machine learning imports
from sklearn.feature_selection import VarianceThreshold, SelectFromModel, f_regression, SelectKBest
from sklearn.preprocessing import StandardScaler
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.model_selection import KFold, train_test_split, cross_validate
from sklearn import model_selection
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
from sklearn.tree import DecisionTreeRegressor
import statsmodels.api as statsmodels
from sklearn.multioutput import RegressorChain

# custom function imports 
from ipynb.fs.full.Getting_Data import GET_from_Excel
from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns
from ipynb.fs.full.Viewing_Dataframe_Stats import viewing_overall_df_stats

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Getting the base features for this model. Looking at the basic overview of the statistics.

# COMMAND ----------

base_feature_df = GET_from_Excel(Configuration_File.PROJECT_START_DATE_DELAY_FEATURE_BASE, "csv")\
                    .drop(['Quote Line Change Order Difference (CAD)', 'Project Detail Primary Marking Count',
                          'Project Detail Remaining Usage', 
                          'Quote Line Additional Disc.', 'Quote Line Total Discount (%)'],1)

# COMMAND ----------

base_feature_df

# COMMAND ----------

base_feature_df = base_feature_df.set_index('Project Detail Record ID')

# COMMAND ----------

cat_col_ls = ['Quote Line Product Code', 'Project Detail Service Delivery Location Country',
              'Project Detail Service Delivery Location City', 'Quote Line Service','Quote Line Subservice',
              'Quote Line GBU', 'Cross and Prodct Market Segment']

# COMMAND ----------

# for col in cat_col_ls:
#     base_feature_df[col+"_encoding_Y1"] = base_feature_df[col]
#     base_feature_df = pd.get_dummies(data=base_feature_df, columns=[col])

# COMMAND ----------

# cat_col_ls = [cat+"_encoding_Y1" for cat in cat_col_ls]

# COMMAND ----------

base_feature_df

# COMMAND ----------

target_columns_ls = ['Hold Days in 0-10% of Project','Hold Days in 10-20% of Project','Hold Days in 20-30% of Project', 
                      'Hold Days in 30-40% of Project', 'Hold Days in 40-50% of Project','Hold Days in 50-60% of Project', 
                      'Hold Days in 60-70% of Project', 'Hold Days in 70-80% of Project', 'Hold Days in 80-90% of Project',
                      'Hold Days in 90-100% of Project']
X = base_feature_df.drop(target_columns_ls,1)
y = base_feature_df[target_columns_ls]

# COMMAND ----------

# split base features into train, validation and test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=1)
#cv_split

# COMMAND ----------

# MAGIC %md
# MAGIC ## Categorical encoding
# MAGIC ### Mapping Features

# COMMAND ----------

kf = KFold(n_splits=30)
kf.get_n_splits(X_train)

# COMMAND ----------

mapping_cat_dic = {}
for train_index, group_to_ignore_index in kf.split(X_train, y_train):
    temp_df = pd.merge(X_train.iloc[train_index], y_train.iloc[train_index], left_index=True, right_index=True)
    for col in cat_col_ls:
        mean_mapping = temp_df.groupby(col)['Hold Days in 0-10% of Project'].mean().to_dict()
        if col in mapping_cat_dic:
            current_dic_map = mapping_cat_dic[col]
            mapping_cat_dic[col] = {k: (current_dic_map.get(k, 0) + mean_mapping.get(k, 0)) 
                                    for k in set(current_dic_map) | set(mean_mapping)}
        else:
            mapping_cat_dic[col] = mean_mapping

# COMMAND ----------

for col_key,dictionary_value in mapping_cat_dic.items():
    for key,value in dictionary_value.items():
        dictionary_value[key] = value/30

# COMMAND ----------

for key,value in mapping_cat_dic.items():
    print(key)
    X_train[key] = X_train[key].replace(value)
    
    X_val[key] = X_val[key].replace(value)
    if X_val[key].dtype != np.number:
        X_val[key] = X_val[key].replace(regex={r'\D+':0})
    
    X_test[key] = X_test[key].replace(value)
    if X_test[key].dtype != np.number:
        X_test[key] = X_test[key].replace(regex={r'\D+':0})

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Scaling_Features'></a>
# MAGIC 
# MAGIC ## Scaling Features 
# MAGIC ### As I will be running a classification models most models benefit from feature scaling. As a result we will scale any features using the standard scaler approach: x-mean/std

# COMMAND ----------

# # Standard Scaler
# scaler_obj = StandardScaler()

# # fit on training
# scaler_obj.fit(X_train)

# # transform data sets
# X_train[X_train.columns] = scaler_obj.transform(X_train[X_train.columns])
# X_val[X_val.columns] = scaler_obj.transform(X_val[X_val.columns])
# X_test[X_test.columns] = scaler_obj.transform(X_test[X_test.columns])

# COMMAND ----------

X_train

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Feature_Selection_Filter'></a>
# MAGIC 
# MAGIC ## Data Feature Selection - Filter Methods
# MAGIC ### Below we will use the filter methods: varience, correlation maps, information gain and fisher score to filter down our feature set. Currently our feature table ahs over 300 features. Not all of these will be helpful to our model.

# COMMAND ----------

# getting rid of unhelpful columns - removing constant features
unhelpful_col_dic = flagging_unhelpful_columns(X_train)

# COMMAND ----------

unhelpful_col_dic

# COMMAND ----------

X_train = X_train.drop(list(unhelpful_col_dic.keys()),1)

# COMMAND ----------

# removing quasi-constant features, features dominanted by one value
quasi_constant_features_obj = VarianceThreshold(threshold=0.02)
quasi_constant_features_obj.fit(X_train)

quasi_constant_ls = list(X_train.columns[quasi_constant_features_obj.get_support()])

# COMMAND ----------

# almost
sns.countplot(x='Quote Line Location_India', data=X_train)

# COMMAND ----------

X_train = X_train[quasi_constant_ls]

# COMMAND ----------

#pip install seaborn_qqplot

# COMMAND ----------



# COMMAND ----------

# correlation
# Sample figsize in inches
fig, ax = plt.subplots(figsize=(20,10))         
# Imbalanced DataFrame Correlation
corr = X_train.corr()
sns.heatmap(corr, cmap='YlGnBu', annot_kws={'size':30}, ax=ax)
ax.set_title("Correlation Matrix", fontsize=14)
plt.show()

# COMMAND ----------

c = X_train.corr().abs()

s = c.unstack()
so = s.sort_values(kind="quicksort")

# COMMAND ----------

correlation_df = so.to_frame().reset_index()
correlation_df.columns = ['Column 1', 'Column 2', 'Correlation']
correlation_df = correlation_df[(correlation_df['Correlation']>= 0.8) 
                                & (correlation_df['Column 1']!=correlation_df['Column 2'])]
correlation_df = correlation_df.iloc[::2]
correlation_df

# COMMAND ----------

X_train = X_train.drop(list(correlation_df['Column 1'].unique()),1)

# COMMAND ----------

variables = range(X.shape[1])
dropped=True
while dropped:
    dropped=False
    vif = [variance_inflation_factor(X_train.values, ix) for ix in range(X_train.shape[1])]

    maxloc = vif.index(max(vif))
    print(max(vif))
    if max(vif) > 10:
        print('dropping \'' + X_train.columns[maxloc] + '\' at index: ' + str(maxloc))
        X_train = X_train.drop(X_train.columns[maxloc],1)
        dropped=True

# COMMAND ----------

X_train

# COMMAND ----------

# MAGIC %md
# MAGIC ## Chain Regressors - First y column
# MAGIC ### Chain Regressors build off the first y so we will try to reduce the space by leveraging the first y column

# COMMAND ----------

# y_train_first_chain = np.ravel(y_train[['Hold Days in 0-10% of Project']])
# y_train_first_chain.shape

# COMMAND ----------

# # configure to select all features
# selectorK = SelectKBest(f_regression)
# feature_score_p_value_ls =  selectorK.fit_transform(X_train, y_train_first_chain)
# feature_score_p_value_ls

# COMMAND ----------

# feature_score_p_value_ls[0]

# COMMAND ----------

# selectorK.get_support() #list of booleans


# COMMAND ----------

# K_selected_features = list(X_train.columns[selectorK.get_support()])

# COMMAND ----------

# X_train = X_train[K_selected_features]

# COMMAND ----------

X_linear_tester_train = pd.merge(y_train[['Hold Days in 0-10% of Project']], X_train, left_index=True, right_index=True)

# COMMAND ----------

sns.pairplot(X_linear_tester_train, size=5, aspect=0.9)

# COMMAND ----------

sns.pairplot(X_linear_tester_train, x_vars=X_linear_tester_train.columns,
             y_vars='Hold Days in 0-10% of Project', size=5, aspect=0.9)

# COMMAND ----------

# X_linear_tester_train.groupby(['Project Detail Sample Count']).nunique()

# COMMAND ----------

X_train = X_train[X_train['Quote Line Admin and General PF total']<200000]

# COMMAND ----------

# sns.pairplot(X_linear_tester_train, size=5, aspect=0.9)

# COMMAND ----------

from seaborn_qqplot import pplot
from scipy.stats import norm

# pplot(X_train, x='Project Detail Sample Count', y=norm, kind='qq', height=4, aspect=2)

# COMMAND ----------

# sns.distplot(X_train['Project Detail Days to Project Close'])

# COMMAND ----------

# X_train = X_train[X_train['Project Detail Sample Count'] < 3]

# COMMAND ----------

# relevant_index = X_train.index
# relevant_index
# y_train = y_train[y_train.index.isin(relevant_index)]

# COMMAND ----------

# sample_mean_num = X_train['Project Detail Days to Project Close'].mean()
# X_train['Project Detail Days to Project Close'] = X_train['Project Detail Days to Project Close'].replace(-1,sample_mean_num).replace(0,sample_mean_num)

# COMMAND ----------

# from scipy import stats
# X_train['Project Detail Days to Project Close'],fitted_lambda = stats.boxcox(X_train['Project Detail Days to Project Close'])

# COMMAND ----------

# X_train[X_train['Project Detail Days to Project Close']<=0]

# COMMAND ----------

# X_test['Project Detail Days to Project Close'] = X_test['Project Detail Days to Project Close'].replace(0,sample_mean_num).replace(-1,sample_mean_num)
# X_test['Project Detail Days to Project Close'] = stats.boxcox(X_test['Project Detail Days to Project Close'], fitted_lambda)

# COMMAND ----------



# COMMAND ----------

X_base_train = statsmodels.add_constant(X_train)
y_base_train = y_train[['Hold Days in 0-10% of Project']]

# COMMAND ----------

base_model = statsmodels.OLS(y_base_train, X_base_train).fit()

# COMMAND ----------

base_model.summary()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Cross_Comparing_Models'></a>
# MAGIC 
# MAGIC ## Cross Comparing Models 
# MAGIC 
# MAGIC ### Running a basic comparision of classification models. To guage which model will out perform the others based on just the feature selection list.

# COMMAND ----------

selected_columns_model_ls = list(X_train.columns)
X_test = X_test[selected_columns_model_ls]

# COMMAND ----------

models_ls = [ ("LinearRegression", LinearRegression()), 
              ("Ridge", Ridge()),
              ("Lasso", Lasso()),
              ("ElasticNet", ElasticNet()),
              ("DecisionTreeRegressor", DecisionTreeRegressor())]

results = []
names = []
final_scores_df_ls = []
scoring_ls = ['neg_mean_squared_error', 'neg_mean_absolute_error', 'explained_variance', 'r2']

for name, model in models_ls:
        
    kfold = model_selection.KFold(n_splits=10, shuffle=True, random_state=90210)
    clf_chain = RegressorChain(base_estimator=model, order=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).fit(X_train, y_train)
    
    cv_results = model_selection.cross_validate(clf_chain, X_train, y_train, cv=kfold, scoring=scoring_ls)
    
    y_pred = clf_chain.predict(X_test)
    print(name)
    print(clf_chain)
    results.append(cv_results)
    names.append(name)
    this_df = pd.DataFrame(cv_results)
    this_df['model'] = name
    final_scores_df_ls.append(this_df)

final = pd.concat(final_scores_df_ls, ignore_index=True)

# COMMAND ----------

final

# COMMAND ----------

final_model = RegressorChain(base_estimator=LinearRegression(), order=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Determining_Model_Parameters'></a>
# MAGIC 
# MAGIC ## Selecting Model Parameters
# MAGIC ### Now that we have a base model we can grid search for model parameters that can improve our model. Im using the validation set to determine parameter set.

# COMMAND ----------

skf = model_selection.KFold(n_splits=10, shuffle=True, random_state=90210)

# COMMAND ----------

RegressorChain(LinearRegression()).get_params() 

# COMMAND ----------

from sklearn.model_selection import GridSearchCV
params = {
    'base_estimator__fit_intercept': [True, False],
    'base_estimator__normalize':[True, False],
    'base_estimator__copy_X':[True, False],
    'base_estimator__n_jobs': [1,5,10, None]
}

grid_no_up = GridSearchCV(final_model, param_grid=params, cv=skf, 
                          scoring=scoring_ls, refit='r2').fit(X_train, y_train)


# COMMAND ----------

# best suggested parameters based on grid search
best_params_lr_dic = grid_no_up.best_params_

# COMMAND ----------

for key, value in best_params_lr_dic.items():
    best_params_lr_dic[key.replace("base_estimator__","")] = best_params_lr_dic.pop(key)

# COMMAND ----------

best_params_lr_dic

# COMMAND ----------

# final models score should beat this one
grid_no_up.best_score_

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Fitting_Final_Model'></a>
# MAGIC 
# MAGIC ## Fitting Final Model
# MAGIC ### Setting up and fitting our final model with our best parameters.

# COMMAND ----------

final_model = RegressorChain(base_estimator = LinearRegression(copy_X= True, n_jobs= 1, normalize= True, fit_intercept= True), 
                             order=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

# COMMAND ----------

# fit the model
final_model.fit(X_train, y_train)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Feature_Importance'></a>
# MAGIC 
# MAGIC ## Feature Importance
# MAGIC ### Seeing the weights associated to each of the features will allow me to see what influences the model the most.

# COMMAND ----------

# get importance
importance = final_model.estimators_[0]
# summarize feature importance
for i,v in enumerate(importance.coef_):
    print('Feature: %0s, Index: %.5f, Score: %.5f' % (X_train.columns[i], i,v))
# plot feature importance
plt.bar([x for x in range(len(importance.coef_))], importance.coef_)
plt.show()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Final_Model_Predictions'></a>
# MAGIC 
# MAGIC ## Predicting Final Model On Test Data
# MAGIC ### Checking prediction of final model on test data.

# COMMAND ----------

y_test_pred = final_model.predict(X_test)

# COMMAND ----------

y_test_pred[0]

# COMMAND ----------

y_test.values[0]

# COMMAND ----------

for x in list(range(0,10)):
    print("R squared: {}".format(r2_score(y_true=y_test.values[x],y_pred=y_test_pred[x])))
    residuals = y_test.values[x]-y_test_pred[x]
    mean_residuals = np.mean(residuals)
    print("Mean of Residuals {}".format(mean_residuals))
    sns.distplot(residuals,kde=True)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Saving_Model_and_Parameters'></a>
# MAGIC 
# MAGIC ## Saving Model
# MAGIC ### Saving our  model so it can be re run

# COMMAND ----------

# mapping of categorical variables, scaler then selected column and final trained model
# all_model_info = [mapping_cat_dic, scaler_obj, selected_columns_model_ls, final_model]

# COMMAND ----------

# save the model to disk
# pickle.dump(all_model_info, open(Configuration_File.SALES_QUOTE_ACCPTENCE_MODEL, 'wb'))

# COMMAND ----------



