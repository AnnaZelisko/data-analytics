# Databricks notebook source
# MAGIC %md
# MAGIC # Base Quote Line Table
# MAGIC ### Cleaning up the Salesforce quote line table. Merging clean quote line table with engineered Quote features.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Cleaning_Up_Table">Cleaning Up Table</a><br>
# MAGIC <a href="#Transforming_Data_Columns">Transforming Data Columns</a><br> 
# MAGIC <a href="#Merge_Any_Additional_Features_with_Base">Merge Additional Features</a><br> 

# COMMAND ----------

# MAGIC %md this is a comment

# COMMAND ----------

import numpy as np
import pandas as pd

from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns
from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL, GET_from_Excel
from ipynb.fs.full.Column_Transformations import converting_currency_to_CAD, MERGE_Features_to_Base

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote Line data from SQL database table. Quote line subservice has the relevant Subservice information.

# COMMAND ----------

where_clause_ls = ' WHERE "Quote Line Product Family" =' +"'Subservice'"
quoteline_info_df = GET_from_Centra_SQL(Configuration_File.QUOTELINE, [], where_clause_ls)

# COMMAND ----------

quoteline_info_df

# COMMAND ----------

print("hi")

# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Cleaning_Up_Table"></a>
# MAGIC 
# MAGIC 
# MAGIC ## Cleaning Up Table
# MAGIC ### Removing columns with no information, only one value, duplicates or columns which use are intended for user or developers purposes. i.e. output to the screen, errors

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(quoteline_info_df)

# COMMAND ----------

columns_for_inspection_dic

# COMMAND ----------

# this stores footer information which is not that relevant
screen_output_ls = []
for col in quoteline_info_df.columns:
    if ("Footer" in col):
        screen_output_ls.append(col)

# COMMAND ----------

screen_output_ls

# COMMAND ----------

# 'Quote Line Currency From Account','Quote Line Currency From Opp', we have Quote Line Currency
# Quote Line Global Business Unit as 
unnecessary_information_ls = ['Quote Line Detail Link', 'Quote Line Factory Name - City - Country', 
                           'Quote Line zScope Check Status','Quote Line Scope Check Message', 
                           'Quote Line Product Description Output', 'Quote Line zPrimary Class',
                           'Quote Line Price Type Description Output', 'Quote Line Billing Type Description Output',
                            'Quote Line Currency From Account','Quote Line Currency From Opp',]

# COMMAND ----------

quoteline_info_df = quoteline_info_df.drop(list(columns_for_inspection_dic.keys())\
                                           +screen_output_ls+unnecessary_information_ls, 1)

# COMMAND ----------

quoteline_info_df

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Transforming_Data_Columns"></a>
# MAGIC 
# MAGIC ## Transforming Data Columns
# MAGIC ### Not all columns are the correct versions: currency columns need to be converted and date need to be the same formats to ensure we can engineer new features.

# COMMAND ----------

currency_col_ls = []
for col in quoteline_info_df.columns:
    if ("Date" in col) and (quoteline_info_df[col].dtype != "datetime64[ns]")\
        and (col != 'Quote Line Planned Start Date - Quote Create Date'):
            print(col)
            quoteline_info_df[col] = pd.to_datetime(quoteline_info_df[col], errors='ignore')

    if ("Price" in col or "Total" in col or "Cost" in col or "Revenue" in col or "Change Order Difference" in col
       or "Fee" in col or "Rev" in col or "Amount" in col)\
        and (quoteline_info_df[col].dtype == np.float64 or quoteline_info_df[col].dtype == np.float32):
            currency_col_ls.append(col)

# COMMAND ----------

currency_col_ls.remove('Quote Line Total Discount (%)')

# COMMAND ----------

quoteline_info_df['Cross and Prodct Market Segment'] = quoteline_info_df.apply(lambda row: row['Cross Market Segment'] 
                            if row['Cross Market Segment'] is not None 
                            else row['Market Segment (Prd/Crs Mrkt)'],1)

# COMMAND ----------

quoteline_info_df = converting_currency_to_CAD(quoteline_info_df, currency_col_ls, "Quote Line Currency")\
                    .drop(currency_col_ls+['Market Segment (Prd/Crs Mrkt)', 'Cross Market Segment'],1)

# COMMAND ----------

quoteline_info_df

# COMMAND ----------

for x in quoteline_info_df.columns:
    if quoteline_info_df[x].isnull().sum() >0:
        print(x)
        print(quoteline_info_df[x].isnull().sum())
        print(quoteline_info_df[x].dtype)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Merge_Any_Additional_Quote_Features_with_Our_Quote_Base"></a>
# MAGIC 
# MAGIC ## Merge Any Additional Quote Features with Our  Base
# MAGIC ### Adding together any additional features that were created to the base

# COMMAND ----------

quoteline_info_df = MERGE_Features_to_Base(quoteline_info_df, "QUOTELINES", "Quote Line Record ID")

# COMMAND ----------

for x in quoteline_info_df.columns:
    if quoteline_info_df[x].isnull().sum() >0:
        print(x)
        print(quoteline_info_df[x].isnull().sum())
        print(quoteline_info_df[x].dtype)

# COMMAND ----------

quoteline_info_df['Quote Line Customer Product Description'] = quoteline_info_df['Quote Line Customer Product Description']\
                                                                    .apply(lambda value: len(value) 
                                                                           if value is not None else 0,1)

# COMMAND ----------

quoteline_info_df.to_csv(Configuration_File.QUOTELINE_BASE_BUSINESS, index = None, header=True)

# COMMAND ----------


