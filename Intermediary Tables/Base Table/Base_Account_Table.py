# Databricks notebook source
# MAGIC %md
# MAGIC # Base Project Table
# MAGIC ### Cleaning up the Salesforce project table.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Cleaning_Up_Project_Table">Cleaning Up Project Table</a><br>
# MAGIC <a href="#Transforming_Data_Columns">Transforming Data Columns</a><br>
# MAGIC <a href="#Engineering_New_Features_Based_on_Base_Table">Engineering New Features Based on Base_Table</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = "..\\" * dir_to_data
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import pandas as pd
from datetime import datetime, timedelta
import numpy as np

from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns
from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL, GET_from_Excel
from ipynb.fs.full.Column_Transformations import converting_currency_to_CAD, MERGE_Features_to_Base

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote data from SQL database table.

# COMMAND ----------

account_df = GET_from_Centra_SQL(Configuration_File.ACCOUNT)

# COMMAND ----------

# every project should be associated to a customer
project_info_df = project_info_df[project_info_df['Project Detail Customer'].notnull()]

# COMMAND ----------

project_info_df

# COMMAND ----------

project_info_df['Project Detail Technical/Certification Reviewer Required']\
= project_info_df['Project Detail Technical/Certification Reviewer'].apply(lambda value:1 if value is not None else 0,1)

# COMMAND ----------

project_info_df['Project Detail Evaluation Checker Required']\
= project_info_df['Project Detail Evaluation Checker'].apply(lambda value:1 if value is not None else 0,1)

# COMMAND ----------

project_info_df['Project Detail Certificate Provided']\
= project_info_df['Project Detail Certificate'].apply(lambda value:1 if value is not None else 0,1)

# COMMAND ----------

project_info_df['Project Detail Owned by my team'] = project_info_df['Project Detail Owned by my team']\
                                                     .apply(lambda value:1 if value == True else 0,1)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Cleaning_Up_Project_Table"></a>
# MAGIC 
# MAGIC 
# MAGIC ## Cleaning Up Project Table
# MAGIC ### Removing columns with no information, only one value, duplicates or columns which use are intended for user or developers purposes. i.e. output to the screen, errors

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(project_info_df)

# COMMAND ----------

columns_for_inspection_dic

# COMMAND ----------

# removed from dictionary due to value
columns_for_inspection_dic.pop('Project Detail Certificate ID', None)
columns_for_inspection_dic.pop('Project Detail Certificate', None)
columns_for_inspection_dic.pop('Project Detail Element Proposal Id', None)
columns_for_inspection_dic.pop('Project Detail Main Project Detail', None)

# COMMAND ----------

# Project Detail Service Delivery Location - Reporting is a replacement for Project Detail Service Delivery Location/A
# Project Detail Draft Report Issued Milestone Date - Reporting > Project Detail Draft Report Issued Milestone Date
# Project Detail Final Report Issued Date - Reporting > Project Detail Final Report Issued Date
# Project Detail L1 Task Reporting > Project Detail L1 Task
# Project Detail Project Close Date we have Project Detail Project Close Date Reporting
# Project Detail Last Completed - Task:Milestone we have Project Detail Last Completed - Task:Milestone Reporting
unnecessary_information_ls = ['Project Detail Project Bin Link', 'Project Detail Project Bin', 
                              'Project Detail Service Delivery Location', 'Project Detail Service Delivery Location A',
                              'Project Detail Draft Report Issued Milestone Date', 'Project Detail Draft Report Issue Date',
                              'Project Detail Final Report Issued Date', 'Project Detail L1 Task', 
                              'Project Detail Project Close Date', 'Project Detail Last Completed - Task:Milestone',
                              'Project Detail Last Completed - Task:Milestone']

# COMMAND ----------

project_info_df = project_info_df.drop(list(columns_for_inspection_dic.keys())+unnecessary_information_ls, 1)

# COMMAND ----------

project_info_df

# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Transforming_Data_Columns"></a>
# MAGIC 
# MAGIC ## Transforming Data Columns
# MAGIC ### Not all columns are the correct versions: currency columns need to be converted and date need to be the same formats to ensure we can engineer new features.

# COMMAND ----------

currency_col_ls = []
for col in project_info_df.columns:
    if ("Date" in col or "Modstamp" in col) and project_info_df[col].dtype != "datetime64[ns]":
        print(col)
        project_info_df[col] = pd.to_datetime(project_info_df[col], errors='ignore')
    if "Amount" in col:
        currency_col_ls.append(col)
    

# COMMAND ----------

project_info_df = converting_currency_to_CAD(project_info_df, currency_col_ls, "Project Detail Currency")\
                    .drop(currency_col_ls,1)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Engineering_New_Features_Based_on_Base_Table"></a>
# MAGIC 
# MAGIC ## Engineering New Project Features Based on Base Table
# MAGIC ### Some additional cross features based on the ones put together.

# COMMAND ----------

project_info_df[['Project Detail Service Delivery Location Country','Project Detail Service Delivery Location City']] = \
    project_info_df['Project Detail Service Delivery Location - Reporting'].str.split(" - ",1, expand=True)

# COMMAND ----------

project_info_df = project_info_df.drop('Project Detail Service Delivery Location - Reporting',1)

# COMMAND ----------

project_info_df['Project Length: Day Created to Closed'] = (project_info_df['Project Detail Project Close Date Reporting']\
                                                            - project_info_df['Project Detail Created Date']).dt.days

# COMMAND ----------



# COMMAND ----------

project_info_df['Project Scope Length'] = len(project_info_df['Project Detail Scope'])

# COMMAND ----------

project_info_df

# COMMAND ----------

project_info_df.to_csv(Configuration_File.PROJECT_BASE_BUSINESS, index = None, header=True)

# COMMAND ----------


