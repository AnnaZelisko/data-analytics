# Databricks notebook source
# MAGIC %md
# MAGIC # Base Quote Table
# MAGIC ### Cleaning up the Salesforce quote table. Merging clean quote table with engineered Quote features.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Cleaning_Up_Quote_Table">Cleaning Up Quote Table</a><br>
# MAGIC <a href="#Transforming_Data_Columns">Transforming Data Columns</a><br>
# MAGIC <a href="#Engineering_New_Quote_Features_Based_on_Base_Table">Engineering New Quote Features Based on Base_Table</a><br> 
# MAGIC <a href="#Merge_Any_Additional_Quote_Features_with_Our_Quote_Base">Merge Additional Quote Features with Quote</a><br> 

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = ''
for x in range(0,dir_to_data):
    string += '..\\'
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import pandas as pd
from datetime import datetime, timedelta
import numpy as np

from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns
from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL, GET_from_Excel
from ipynb.fs.full.Column_Transformations import converting_currency_to_CAD, MERGE_Features_to_Base

import Configuration_File

# COMMAND ----------

TODAY = datetime.today()

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote data from SQL database table.

# COMMAND ----------

quote_info_df = GET_from_Centra_SQL(Configuration_File.QUOTE)

# COMMAND ----------

# every quote should be associated to a customer
quote_info_df = quote_info_df[quote_info_df['Quote Account'].notnull()]

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Cleaning_Up_Quote_Table"></a>
# MAGIC 
# MAGIC 
# MAGIC ## Cleaning Up Quote Table
# MAGIC ### Removing columns with no information, only one value, duplicates or columns which use are intended for user or developers purposes. i.e. output to the screen, errors

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(quote_info_df)

# COMMAND ----------

columns_for_inspection_dic

# COMMAND ----------

# this stores footer information which is not that relevant
screen_output_ls = []
for col in quote_info_df.columns:
    if "Footer" in col:
        screen_output_ls.append(col)

# COMMAND ----------

screen_output_ls

# COMMAND ----------

unnecessary_information_ls = ['Quote Agent Contact Details Output', 'Quote Account Address Output', 
                              'Quote Output Bill to City','Quote Output Bill to Country', 
                              'Quote Agent Exists Output Inverse', 'Quote Salutation Output', 'Quote Today Date',
                              'Quote Approval Status','Quote Scope English Exists','Quote Scope Exists Output',
                              'Quote Scope Non English Exists', 'Quote Scope Count Output', 'Quote EditLinesFieldSetName',
                              'Quote Opportunity Name','Quote Watermark Shown', 'Quote Approval Required', 
                              'Quote Dynamic Logo Generation Output', 'Quote Samples Count Exists Output',
                              'Quote Bill to Customer Status','Quote Exchange Rate Applied', 'Quote zQuote Doc Count',
                              'Quote Primary Service Delivery Location Text']

# COMMAND ----------

quote_info_df = quote_info_df.drop(list(columns_for_inspection_dic.keys())+screen_output_ls+unnecessary_information_ls, 1)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Transforming_Data_Columns"></a>
# MAGIC 
# MAGIC ## Transforming Data Columns
# MAGIC ### Not all columns are the correct versions: currency columns need to be converted and date need to be the same formats to ensure we can engineer new features.

# COMMAND ----------

currency_col_ls = []
for col in quote_info_df.columns:
    if ("Date" in col or "Expires On" in col) and quote_info_df[col].dtype != "datetime64[ns]":
        print(col)
        quote_info_df[col] = pd.to_datetime(quote_info_df[col], errors='ignore')
    if "Amount" in col:
        currency_col_ls.append(col)
    

# COMMAND ----------

quote_info_df = converting_currency_to_CAD(quote_info_df, currency_col_ls, "Quote Currency").drop(currency_col_ls,1)

# COMMAND ----------

# Quote Expires on is null in some cases, based on discussions with Dejan this should be next 90 days after created
quote_info_df.loc[quote_info_df['Quote Expires On'].isnull(),
                  'Quote Expires On'] = quote_info_df['Quote Created Date'] + timedelta(days=90)

# COMMAND ----------

# not all strings are Capital then lower case
quote_info_df['Quote Bill To City'] = quote_info_df['Quote Bill To City'].str.title()
quote_info_df['Quote Ship To City'] = quote_info_df['Quote Ship To City'].str.title()
quote_info_df['Quote Bill To City'] = quote_info_df['Quote Bill To City'].replace(".", np.nan)
quote_info_df['Quote Ship To City'] = quote_info_df['Quote Ship To City'].replace(".", np.nan)

# some states have states, number 
quote_info_df['Quote Bill To State'] = quote_info_df['Quote Bill To State'].str.split(",").str[0]
quote_info_df['Quote Bill To State'] = quote_info_df['Quote Bill To State'].replace(".", np.nan)

quote_info_df['Quote Ship To State'] = quote_info_df['Quote Ship To State'].str.split(",").str[0]
quote_info_df['Quote Ship To State'] = quote_info_df['Quote Ship To State'].replace(".", np.nan)

# COMMAND ----------

for x in quote_info_df.columns:
    if quote_info_df[x].isnull().sum() >0:
        print(x)
        print(quote_info_df[x].isnull().sum())

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Engineering_New_Quote_Features_Based_on_Base_Table"></a>
# MAGIC 
# MAGIC ## Engineering New Quote Features Based on Base Table
# MAGIC ### Some additional cross features based on the ones put together.

# COMMAND ----------

quote_info_df['Days Quote Created to Close'] = (quote_info_df['Quote Close Date'] 
                                                - quote_info_df['Quote Created Date']).dt.days

# COMMAND ----------

quote_info_df['Days To Expiration'] = quote_info_df.apply(lambda row: (TODAY - row['Quote Expires On']).dt.days\
                                            if pd.isnull(row['Quote Close Date']) and pd.notnull(row['Quote Expires On'])\
                                            else np.nan,1)

# COMMAND ----------

quote_info_df['Days Outstanding'] = quote_info_df.apply(lambda row:(TODAY - row['Quote Created Date']).days\
                                                    if pd.isnull(row['Quote Close Date'])\
                                                    else np.nan,1)

# COMMAND ----------

quote_info_df[['Quote Region Country','Quote Region City']] = quote_info_df['Quote Region A'].str.split(" ", 1,expand=True)

# COMMAND ----------

quote_info_df[['Quote Primary Service Delivery Location Country','Quote Primary Service Delivery Location City']] = \
    quote_info_df['Quote Primary Service Delivery Location'].str.split(" ",1, expand=True)

# COMMAND ----------

quote_info_df['Quote Bill and Ship To City Are The Same'] = quote_info_df['Quote Bill To City']\
                                                                == quote_info_df['Quote Ship To City'] 

# COMMAND ----------

quote_info_df['Quote Bill and Ship To Country Are The Same'] = quote_info_df['Quote Bill To Country']\
                                                                == quote_info_df['Quote Ship To Country'] 

# COMMAND ----------

quote_info_df['Quote Bill and Ship To Postal Code Are The Same'] = quote_info_df['Quote Bill To Postal Code']\
                                                                == quote_info_df['Quote Ship To Postal Code'] 

# COMMAND ----------

quote_info_df['Quote Bill and Ship To State Are The Same'] = quote_info_df['Quote Bill To State']\
                                                                == quote_info_df['Quote Ship To State'] 

# COMMAND ----------

quote_info_df.loc[(quote_info_df['Quote Customer Status'].isnull()) & (quote_info_df['Quote Created Date']>"2019-01-01"),
                  'Quote Customer Status'] = "Active"
quote_info_df['Quote Customer Status'] = quote_info_df['Quote Customer Status'].fillna("Inactive")

# COMMAND ----------

quote_rejected_ls = ['Quote Cancelled', 'Quote Rejected by Customer']
quote_info_df.loc[(quote_info_df['Quote Win Reasons'].isnull()) & (quote_info_df['Quote Status'].isin(quote_rejected_ls)),
                  'Quote Win Reasons']="Customer Rejected Quote"
quote_info_df['Quote Win Reasons'] = quote_info_df['Quote Win Reasons'].fillna("Unknown")

# COMMAND ----------

quote_info_df['Quote Created By Department'] = quote_info_df['Quote Created By Department'].fillna("Unknown")
quote_info_df['Quote Created By Sales Office'] = quote_info_df['Quote Created By Sales Office'].fillna("Unknown")

# COMMAND ----------

# cleaning up quote competitor column
competitors_df = quote_info_df[['Quote Record ID','Quote Competitors']]
competitors_df['Quote Competitors'] = competitors_df['Quote Competitors'].fillna("Unknown")

# COMMAND ----------

competitors_exploded_df = pd.concat([pd.Series(row['Quote Record ID'], row['Quote Competitors'].split(';'))              
                    for _, row in competitors_df.iterrows()]).reset_index()

competitors_exploded_df.columns = ['Quote Competitors', 'Quote Record ID']
competitors_exploded_df['Quote Competitors'] = "Competitor " +competitors_exploded_df['Quote Competitors']

# COMMAND ----------

competitors_exploded_df['Base Counter'] = 1

# COMMAND ----------

competitors_exploded_df['Quote Competitors'].unique()

# COMMAND ----------

competitors_pivot_df = pd.pivot_table(competitors_exploded_df, index='Quote Record ID', columns='Quote Competitors', 
                                   values='Base Counter', aggfunc="count", fill_value=0).reset_index()

# COMMAND ----------

quote_info_df = pd.merge(quote_info_df, competitors_pivot_df, 
                                           how='left', on='Quote Record ID')\
                  .drop(['Quote Competitors','Quote Region A', 'Quote Primary Service Delivery Location'],1)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id="Merge_Any_Additional_Quote_Features_with_Our_Quote_Base"></a>
# MAGIC 
# MAGIC ## Merge Any Additional Quote Features with Our Quote Base
# MAGIC ### Adding together any additional quote features that were created to the base

# COMMAND ----------

quote_info_df = MERGE_Features_to_Base(quote_info_df, "QUOTES", "Quote Record ID")

# COMMAND ----------

quote_info_df

# COMMAND ----------

quote_info_df.to_csv(Configuration_File.QUOTE_BASE_BUSINESS, index = None, header=True)

# COMMAND ----------


