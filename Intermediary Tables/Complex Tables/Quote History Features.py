# Databricks notebook source
# MAGIC %md
# MAGIC # Quote History
# MAGIC ### Quote History contains stage information but also details about any changes in the quote
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Locked_Unlocked_Quotes">Locked Unlocked Quotes</a><br>
# MAGIC <a href="#Approval_Status">Quote Approval Status</a><br>
# MAGIC <a href="#Expiration_Date">Quote Expiration Date</a><br> 
# MAGIC <a href="#Dynamic_Feature_Engineering">Dynamic Feature Engineering</a><br>
# MAGIC <a href="#Outputing_Table">Outputing Table</a><br> 

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = ''
for x in range(0,dir_to_data):
    string += '..\\'
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import numpy as np
import pandas as pd

from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL

import Configuration_File

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a> 
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote History table from the SQL database, filtering for fields that we want to create additional features on

# COMMAND ----------

cols_for_quote_history_ls = ['Quote History Custom Object Record', 'Quote History Created Date', 'Quote History Changed Field',
                             'Quote History Old Value','Quote History New Value']
quote_status_history_df = GET_from_Centra_SQL(Configuration_File.QUOTEHISTORY, cols_for_quote_history_ls)

# COMMAND ----------

not_field_ls =["SBQQ__Status__c", "created", "Win_Reasons__c", "Service_Delivery_Location__c", "UCB_Error_Message__c",
               "UCB_Error__c", "Lost_Reason_Details__c","Lost_Reason__c", "SBQQ__Primary__c"]
quote_status_history_df = quote_status_history_df[~quote_status_history_df['Quote History Changed Field'].isin(not_field_ls)]

# COMMAND ----------

quote_status_history_df['Quote History Changed Field'].unique()

# COMMAND ----------

final_base_df = quote_status_history_df[['Quote History Custom Object Record']].drop_duplicates()

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Locked_Unlocked_Quotes'></a>
# MAGIC 
# MAGIC ## Handling Locked and Unlocked Quotes
# MAGIC ### Quotes can be locked and unlocked, so this can be a new feature

# COMMAND ----------

quotes_locked_unlocked_df = quote_status_history_df[quote_status_history_df['Quote History Changed Field']\
                                                 .isin(['locked', 'unlocked'])]
quotes_locked_unlocked_df['Base Counter'] = 1

# COMMAND ----------

quotes_locked_unlocked_df = pd.pivot_table(quotes_locked_unlocked_df, values='Base Counter', 
                                           index=['Quote History Custom Object Record'],
                                           columns=['Quote History Changed Field'], aggfunc=np.sum).reset_index()
quotes_locked_unlocked_df.columns = ['Quote History Custom Object Record', 'Locked', 'Unlocked']

# COMMAND ----------

quotes_locked_unlocked_df['Currently Locked'] = quotes_locked_unlocked_df['Locked'] - quotes_locked_unlocked_df['Unlocked']

# COMMAND ----------

final_base_df = pd.merge(final_base_df, quotes_locked_unlocked_df, how='left', on='Quote History Custom Object Record')\
                  .fillna(0)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Approval_Status'></a>
# MAGIC 
# MAGIC ## Approval Status 
# MAGIC ### Some quotes require approval status this can be valuable features

# COMMAND ----------

quotes_approvalstatus_df = quote_status_history_df[
                            (quote_status_history_df['Quote History Changed Field'] == 'ApprovalStatus__c')\
                            & ((quote_status_history_df['Quote History Old Value'] == 'Recalled') \
                               | (quote_status_history_df['Quote History New Value'] == 'Recalled'))]
quotes_approvalstatus_df['Base Counter'] = 1

# COMMAND ----------

quotes_approvalstatus_df = pd.pivot_table(quotes_approvalstatus_df, values=['Base Counter', 'Quote History Created Date'],
                                          index=['Quote History Custom Object Record'], 
                                          columns=['Quote History New Value'],
                                          aggfunc={'Base Counter':np.sum, 'Quote History Created Date':np.max}, 
                                          fill_value=0)\
                                        .reset_index()

quotes_approvalstatus_df.columns = ["Quote History Custom Object Record", "Quote Approved", "Quote Approval Pending",
                                    "Quote Recalled", "Quote Approval Rejected","Quote Approved Date", 
                                    "Quote Approval Pending Date", "Quote Recalled Date", "Quote Approval Rejected Date"] 

# COMMAND ----------

quotes_approvalstatus_df 

# COMMAND ----------

final_base_df = pd.merge(final_base_df, quotes_approvalstatus_df, how='left', on='Quote History Custom Object Record')

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Expiration_Date'></a>
# MAGIC 
# MAGIC ## Expiration Date
# MAGIC ### The expiration date for some of the quotes gets pushed out over time. 

# COMMAND ----------

quotes_experation_df = quote_status_history_df[quote_status_history_df['Quote History Changed Field'] 
                                                   == 'SBQQ__ExpirationDate__c']
quotes_experation_df['Base Counter'] = 1

# COMMAND ----------

quotes_experation_df = pd.pivot_table(quotes_experation_df, values=['Base Counter', 'Quote History Created Date'],
                                       index=['Quote History Custom Object Record'],
                                       aggfunc={'Base Counter':np.sum, 'Quote History Created Date':[np.max, np.min]}, 
                                       fill_value=0)\
                                        .reset_index()

quotes_experation_df.columns = ["Quote History Custom Object Record", "Quote Extended Expiration", 
                                        "Quote Earliest Expiration", "Quote Latest Expiration"] 

# COMMAND ----------

final_base_df = pd.merge(final_base_df, quotes_experation_df, how='left', on='Quote History Custom Object Record')

# COMMAND ----------



# COMMAND ----------

quote_status_history_df = quote_status_history_df[~quote_status_history_df['Quote History Changed Field'].isin(['locked', 'unlocked', 'ApprovalStatus__c'])]

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Dynamic_Feature_Engineering'></a>
# MAGIC 
# MAGIC ## Dynamic Feature Engineering 
# MAGIC ### Creating new features dynamically based on the existing statuses

# COMMAND ----------

status_column_field = list(quote_status_history_df['Quote History Changed Field'].unique())

for status in status_column_field:
    print(status)
    temp_all_status = quote_status_history_df[(quote_status_history_df['Quote History Changed Field']==status) \
                                           & (quote_status_history_df['Quote History Old Value'].notnull())]\
                                           ['Quote History Custom Object Record'].unique()
    
    col_name = status.replace("__c","").replace("SBQQ__","")+" Changed"
    final_base_df[col_name] = final_base_df['Quote History Custom Object Record']\
                                .apply(lambda value: 1 if value in temp_all_status else 0,1)
    

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Outputing_Table'></a>
# MAGIC 
# MAGIC ## Outputting table

# COMMAND ----------

final_base_df = final_base_df.rename(columns={'Quote History Custom Object Record':'Quote Record ID'})

# COMMAND ----------

final_base_df.to_csv(Configuration_File.QUOTES_HISTORY_DETAILS, index = None, header=True)

# COMMAND ----------


