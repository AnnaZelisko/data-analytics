# Databricks notebook source
from string import ascii_letters
import numpy as np
import pandas as pd
import seaborn as sns
#import matplotlib.pyplot as plt
import pyodbc 

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 100000)

# COMMAND ----------

brad_conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=rexsqlp04;'
                      'Database=CENTRA;'
                      'Trusted_Connection=yes;')

# COMMAND ----------

# # Returns columns with less than 70% full or that have only 1 value, as these columns wont inform us of anything new
def columns_not_needed(df):
    # columns that are more than 70% empty as they are not informing us of anything
    null_columns = df.loc[:,list(round(100*(df.isnull().sum()/len(df.index)), 2)>70)].columns.to_list()
    print(len(null_columns))
    
    # no values to compare as there is only one value
    for x in df.columns:
        if df[x].nunique() < 2:
            print(x)
            print(df[x].unique())
            null_columns.append(x)

    return null_columns
    

# COMMAND ----------

Account_columns = ['BillingCity',
 'BillingCountry',
 'BillingPostalCode',
 'BillingState',
 'BillingStreet',
 'C4C_Enabled__c',
 'Commercial_Unit__c',
 'Country_Lookup__c',
 'CreatedDate',
 'CurrencyIsoCode',
 'Customer_Status__c',
 'Exclude_from_Annual_Fees__c',
 'Exclude_from_Quarterly_Fees__c',
 'Global_Business_Unit__c',
 'Id',
 'Industry',
 'KimbleOne__IsCustomer__c',
 'Last_Statistic_Recalculation_Date__c',
 'LastActivityDate',
 'MasterContractCount__c',
 'Name',
 'NumberOfEmployees',
 'Ownership',
 'Phone',
 'Preferred_Communication_Language__c',
 'RateCard__c',
 'RecordTypeId',
 'Region_3__c',
 'RegionFormula__c',
 'Sales_Region__c',
 'Sic',
 'Site',
 'Type',
 'Website',
 'DNBoptimizer__DNB_D_U_N_S_Number__c',
 'DNBoptimizer__DnBCompanyRecord__c']

# COMMAND ----------

from simple_salesforce import Salesforce
sf= Salesforce(username='anna.zelisko@csagroup.org', password='HeadInTheGame1', security_token='zVuCeIhWMNxa6UgJ3PTsYcKV')

# COMMAND ----------

from simple_salesforce.exceptions import SalesforceMalformedRequest

# return Table from Salesforce
def df_from_Salesforce(all_cols_to_get, sqol_table_name):
    Salesforce_df = None
    
    while True:
        try:
            seperator = ", "
            cols_to_get = seperator.join(all_cols_to_get)
            Salesforce_df = pd.DataFrame(sf.query_all("SELECT "+cols_to_get+" FROM "+sqol_table_name)['records']).drop('attributes',1)

        except SalesforceMalformedRequest as e:
            print('exception')
            # we want to replace any columns that dont exsist in one system to another
            replaceCol = str(e.content[0]['message'].split("No such column '")[1].split("' on entity ")[0])
            
            print(replaceCol)
            
            all_cols_to_get.remove(replaceCol)
        else: break
    
    return Salesforce_df

# COMMAND ----------

OG_Salesforce_Account = df_from_Salesforce(Account_columns, "ACCOUNT")

# COMMAND ----------

# one row per account
OG_Salesforce_Account['Customer_Status__c'].unique()

# COMMAND ----------

# we really only care about active and Prospective Customers, we already have lost the Inavtives and Nones are no quote
OG_Salesforce_Account = OG_Salesforce_Account[(OG_Salesforce_Account['Customer_Status__c'] == 'Active') & (OG_Salesforce_Account['CreatedDate'] > '2017-07-29 00:00:01.000')]

# COMMAND ----------

OG_Salesforce_Account[OG_Salesforce_Account["Name"] == "Belmont Instrument LLC"]

# COMMAND ----------

OG_Salesforce_Account['CreatedDate'].min()

# COMMAND ----------

OG_Salesforce_Account['RecordTypeId'].unique()

# COMMAND ----------

# get records that are customer accounts
# OG_Salesforce_Account = OG_Salesforce_Account[OG_Salesforce_Account['RecordTypeId'] == '01241000000uw4QAAQ']
OG_Salesforce_Account

# COMMAND ----------

# one row per account
OG_Salesforce_Account['Id'].nunique()

# COMMAND ----------

OG_Salesforce_Account.columns

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC # DNB Company Record

# COMMAND ----------

DNB_Company_Record_col = ['Id', 'DNBoptimizer__BusinessName__c','DNBoptimizer__DUNSNumber__c','DNBoptimizer__DomesticUltimateBusinessName__c','DNBoptimizer__DomesticUltimateDUNSNumber__c','DNBoptimizer__EmployeesHere__c','DNBoptimizer__EmployeeCountTotal__c','DNBoptimizer__EmployeeCountTrendYear__c','DNBoptimizer__Fortune1000Rank__c','DNBoptimizer__GlobalUltimateBusinessName__c','DNBoptimizer__GlobalUltimateDUNSNumber__c','DNBoptimizer__HeadquarterBusinessName__c','DNBoptimizer__HeadquarterDUNSNumber__c','DNBoptimizer__ImporterExporterDescription__c','DNBoptimizer__LegalStatusCode__c','DNBoptimizer__LegalStructureDescription__c','DNBoptimizer__LocationType__c','DNBoptimizer__MarketableIndicator__c','DNBoptimizer__NAICSCode1__c','DNBoptimizer__NAICSCode2__c','DNBoptimizer__NAICSCode3__c','DNBoptimizer__NAICSDescription1__c','DNBoptimizer__NAICSDescription2__c','DNBoptimizer__NAICSDescription3__c','DNBoptimizer__NumberofFamilyMembers__c','DNBoptimizer__DelistedIndicator__c','DNBoptimizer__OutofBusinessIndicator__c','DNBoptimizer__ParentBusinessName__c','DNBoptimizer__ParentDUNSNumber__c','DNBoptimizer__PrimaryCityName__c','DNBoptimizer__PrimaryCountryCode__c','DNBoptimizer__PrimaryCountryCode_ISO__c','DNBoptimizer__PrimaryLatitude__c','DNBoptimizer__PrimaryStreetAddress__c','DNBoptimizer__PrimaryStreetAddress2__c','DNBoptimizer__PrimaryLongitude__c','DNBoptimizer__PrimaryOwns_RentsCode__c','DNBoptimizer__PrimaryPostalCode__c','DNBoptimizer__PrimarySquareFootage__c','DNBoptimizer__PrimaryStateProvinceName__c','DNBoptimizer__Public_PrivateIndicator__c','DNBoptimizer__RevenueTrendYear__c','DNBoptimizer__SalesVolumeUSDollars__c','DNBoptimizer__SIC4Code1__c','DNBoptimizer__SIC4Code1Description__c','DNBoptimizer__SIC4Code2__c','DNBoptimizer__SIC4Code2Description__c','DNBoptimizer__SIC4Code3__c','DNBoptimizer__SIC4Code3Description__c','DNBoptimizer__SIC8Code1__c','DNBoptimizer__SIC8Code1Description__c','DNBoptimizer__SIC8Code2__c','DNBoptimizer__SIC8Code2Description__c','DNBoptimizer__SIC8Code3__c','DNBoptimizer__SIC8Code3Description__c','DNBoptimizer__SmallBusinessIndicator__c','DNBoptimizer__TelephoneNumber__c','DNBoptimizer__WebAddress__c']

# COMMAND ----------

OG_Salesforce_DNBCompany = df_from_Salesforce(DNB_Company_Record_col, "DNBoptimizer__DnBCompanyRecord__c").rename(columns={'Id':'DNB_Id'})

# COMMAND ----------

OG_Salesforce_DNBCompany.info()

# COMMAND ----------

OG_Salesforce_DNBCompany

# COMMAND ----------

Account_Temp = OG_Salesforce_Account
Account_Temp['CSA Customer'] = 1
pd.merge(Account_Temp, OG_Salesforce_DNBCompany, how='outer', left_on=['DNBoptimizer__DnBCompanyRecord__c'], right_on=['DNB_Id']).drop('DNBoptimizer__DNB_D_U_N_S_Number__c',1).to_excel(r'C:\Users\azelisko\Documents\Power BI Demos\Farukh - Client Accquistion\Python Output - Universe.xlsx', sheet_name = 'Universe', index = None, header=True)

# COMMAND ----------

OG_Salesforce_Account = pd.merge(OG_Salesforce_Account, OG_Salesforce_DNBCompany, how='left', left_on=['DNBoptimizer__DnBCompanyRecord__c'], right_on=['DNB_Id']).drop('DNBoptimizer__DNB_D_U_N_S_Number__c',1)

# COMMAND ----------

OG_Salesforce_Account

# COMMAND ----------

OG_Salesforce_Account.info()

# COMMAND ----------

# MAGIC %md
# MAGIC # Master Contract

# COMMAND ----------

# no filters, we dont want to automatically assign float
OG_MasterContract = pd.read_sql("SELECT * FROM stg.MasterContract__c", brad_conn, coerce_float=False)
OG_MasterContract.info()

# COMMAND ----------

bad_columns = columns_not_needed(OG_MasterContract)
bad_columns.remove('LastActivityDate')

# COMMAND ----------

# SAP_MC_ID__c - SAP id
# MasterContractFlowFormula__c - formula flow
# Master_Contract_CustomerGBU__c from Account
# Master_Contract_Change_Time__c no time change
# Master_Contract_CurrencyIsoCode same as Master_Contract_Currency__c
# Master_Contract_CustomerCurrency__c currency from Account
# Master_Contract_Name same concept as id
uselss_info = ['SAP_MC_ID__c', 'MasterContractFlowFormula__c', 'CustomerGBU__c', 'Change_Time__c', 'CurrencyIsoCode', 'CustomerCurrency__c', 'Name']

# COMMAND ----------

resources = ['ShipTO__c', 'Payer__c']

# COMMAND ----------

personal_staff = ['CreatedById', 'LastModifiedById', 'LastModifiedDate', 'SystemModstamp']

# COMMAND ----------

OG_MasterContract = OG_MasterContract.drop(bad_columns+uselss_info+personal_staff+resources,1)

# COMMAND ----------

OG_MasterContract.columns

# COMMAND ----------

OG_Salesforce_MasterContract = df_from_Salesforce(list(OG_MasterContract.columns), "Mastercontract__c")

# COMMAND ----------

OG_Salesforce_MasterContract.columns = ['Master_Contract_'+col for col in OG_Salesforce_MasterContract.columns]

# COMMAND ----------

OG_Salesforce_MasterContract.info()

# COMMAND ----------

OG_Salesforce_MasterContract

# COMMAND ----------

# MAGIC %md
# MAGIC # Quote

# COMMAND ----------

# no filters, we dont want to automatically assign float
OG_Quote = pd.read_sql("SELECT * FROM stg.SBQQ__Quote__c", brad_conn, coerce_float=False)
OG_Quote.info()

# COMMAND ----------

bad_columns = columns_not_needed(OG_Quote)

# COMMAND ----------

duplicate_columns_to_Account = ['Account_Billing_City_Output__c','Account_Billing_Country__c','Account_Billing_Postal_Code__c','Account_Billing_State_Output__c','Account_Billing_Street_Output__c','Account_Name_Output__c','Account_Owner__c', 'Customer_Number_Output__c', 'CustomerGBU__c', 'CustomerStatus__c', 'Ship_To_Account__c', 'Account_Address_Output__c', 'Bill_to_Name_Output__c', 'BilltoCustomerStatus__c']

# COMMAND ----------

duplicate_columns_to_MasterContract = ['OpportunityName__c', 'OppUpdateTime__c', 'Master_Contract_Custom_to_Text_Output__c', 'Master_Contract_Status__c']

# COMMAND ----------

# Deposit_Percentage_Exists__c tells me if I have a madatory deposit percentage
# IFE_Change_Order__c - we wont be looking at quote change orders
# IsChangeOrder__c
#Quote_Label_Sales_Quote__c
useless = ['Agent_Contact_Details_Output__c', 'Deposit_Percentage_Exists__c', 'Footer_Provision_1_Exists__c', 'Footer_Provision_2_Exists__c', 'Footer_Provision_3_Exists__c', 'Footer_Provision_4_Exists__c', 'Footer_Provision_5_Exists__c', 'Footer_Provision_6_Exists__c', 'Footer_Provision_7_English__c', 'Footer_Provision_7_Exists__c', 'Footer_Provision_8_Exists__c', 'Footer_Provision_9_Exists__c', 'French_Selected_Output__c', 'German_Selected_Output__c', 'Italian_Selected_Output__c','IFE_Change_Order__c', 'IsChangeOrder__c', 'Approval_Comments__c', 'EditLinesFieldSetName__c', 'English_Selected_Output__c', 'Europen_Service_Delivery_Location_w_samp__c', 'Footer_Output_2__c', 'Admin_Change_Order__c', 'Chinese_Service_Delivery_Location__c', 'Chinese_Simple_Selected_Output__c', 'Chinese_Traditional_Selected_Output__c', 'Label_Sales_Quote__c', 'Dynamic_Logo_Generation_Output__c', 'Change_Order__c', 'NOT_Euro_SDL_w_Sam__c','Create_new_master_contract__c', 'Operational_Change_Approved__c', 'SBQQ__LastSavedOn__c', 'SBQQ__LastCalculatedOn__c', 'SBQQ__Opportunity2__c', 'SBQQ__PriceBook__c', 'SBQQ__PricebookId__c', 'Weeks_to_Certify_Exists__c', 'Footer_Provision_1_English__c', 'Footer_Provision_2_Chinese__c', 'Footer_Provision_2_English__c', 'Footer_Provision_3_English__c', 'Footer_Provision_4_English__c', 'Footer_Provision_8_French__c', 'Quote_Document_Template__c', 'Agent_Exists_Output_Inverse__c']

# COMMAND ----------

more_resources = ['Quote_Owner__c', 'Output_Bill_to_Customer_Number__c','Technical_Account_Manager__c', 'SubmittedUser__c', 'Submitter_Manager__c', 'SBQQ__SalesRep__c', 'SBQQ__PrimaryContact__c', 'OwnerId', 'Manager__c', 'InsideSalesRep__c', 'GBUVP__c']

# COMMAND ----------

OG_Quote = OG_Quote.drop(bad_columns + duplicate_columns_to_Account + duplicate_columns_to_MasterContract + useless+personal_staff+more_resources, 1)

# COMMAND ----------

OG_Quote['RecordTypeId'] = 0

# COMMAND ----------

# SBQQ__Account__c to Account Id
# MasterContractCustom__c to Master COntract Id
OG_Quote

# COMMAND ----------

#[Label_Sales_Quote__c]
OG_Salesforce_Quote = df_from_Salesforce(list(OG_Quote.columns), "SBQQ__Quote__c WHERE IsChangeOrder__c = False AND Label_Sales_Quote__c = False")

# COMMAND ----------

OG_Salesforce_Quote['Quote_Status__c'].unique()

# COMMAND ----------

OG_Salesforce_Quote['SBQQ__Status__c'].unique()

# COMMAND ----------

OG_Salesforce_Quote.columns

# COMMAND ----------

# include accounts that have accepted before
Accounts_Accepted_Before = OG_Salesforce_Quote[(OG_Salesforce_Quote['SBQQ__Status__c'] == 'Quote Accepted by Customer')]['SBQQ__Account__c'].to_list()
Accounts_Accepted_Before

# COMMAND ----------

# accounts that have accepted before
OG_Salesforce_Quote = OG_Salesforce_Quote[OG_Salesforce_Quote['SBQQ__Account__c'].isin(Accounts_Accepted_Before)]

# COMMAND ----------

OG_Salesforce_Quote.columns = ['Quote_'+col for col in OG_Salesforce_Quote.columns]

# COMMAND ----------

OG_Salesforce_Quote

# COMMAND ----------

OG_Salesforce_Quote[['Quote_SBQQ__Account__c', 'Quote_MasterContractCustom__c']]

# COMMAND ----------

# MAGIC %md
# MAGIC # Quote Line

# COMMAND ----------

Quote_Line = ['Class_CU__c', 'Class_GBU__c', 'CU__c', 'CreatedDate', 'Global_Business_Unit__c', 'Project_Status__c', 'ProjectCloseDate__c', 'Service__c', 'Id', 'Service_Delivery_Location__c', 'Subservice__c', 'SBQQ__Quote__c', 'Product_Name__c']

# COMMAND ----------

# SBQQ__RequiredBy__c points to the parent quote, between change order quotes
OG_Salesforce_QL = df_from_Salesforce(Quote_Line, "SBQQ__QuoteLine__c WHERE SBQQ__RequiredBy__c = NULL AND Product_Name__c != 'Label Shipping Price'")
OG_Salesforce_QL

# COMMAND ----------

OG_Salesforce_QL = OG_Salesforce_QL[OG_Salesforce_QL['Service__c'].notnull()]

# COMMAND ----------

OG_Salesforce_QL['Service__c'].unique()

# COMMAND ----------

OG_Salesforce_QL = OG_Salesforce_QL[OG_Salesforce_QL['SBQQ__Quote__c'].isin(list(OG_Salesforce_Quote['Quote_Id'].unique()))].sort_values(['SBQQ__Quote__c','Id', 'CreatedDate'])
OG_Salesforce_QL[['SBQQ__Quote__c','Id','Product_Name__c','CreatedDate']]

# COMMAND ----------

OG_Salesforce_QL['SBQQ__Quote__c'].nunique()

# COMMAND ----------

OG_Salesforce_QL['Id'].nunique()

# COMMAND ----------

OG_Salesforce_QL.to_excel(r'C:\Users\azelisko\Documents\Power BI Demos\ELT - CSA Dashboard\Python Output - Quote Lines.xlsx', sheet_name = 'Quotes Lines', index = None, header=True)

# COMMAND ----------

OG_Salesforce_QL.columns = ['Quote_Line_'+colname for colname in OG_Salesforce_QL.columns]

# COMMAND ----------

# MAGIC %md
# MAGIC # Feature Engineering

# COMMAND ----------

OG_Salesforce_Account

# COMMAND ----------

OG_Salesforce_Account["BillingState"] = OG_Salesforce_Account["BillingState"].apply(lambda value: None if pd.isnull(value) else(value.split(',')[0] if ',' in value else value),1)

# COMMAND ----------

OG_Salesforce_Account['BillingCity'] = OG_Salesforce_Account['BillingCity'].replace('[^a-zA-Z0-9 ]', '', regex=True).replace({'CergySaintChristophe Cedex':'Cergy', 'Siheungsi': 'Siheung', 'Georgsmarienhtte': 'Georgsmarienhutte', 'StPaul':'Saint Paul', 'SaintPaul':'Saint Paul'})
OG_Salesforce_Account['BillingCountry'] = OG_Salesforce_Account['BillingCountry'].replace('[^a-zA-Z0-9 ]', '', regex=True)
OG_Salesforce_Account['BillingPostalCode'] = OG_Salesforce_Account['BillingPostalCode'].replace('[^a-zA-Z0-9 ]', '', regex=True)
OG_Salesforce_Account['BillingState'] = OG_Salesforce_Account['BillingState'].replace('[^a-zA-Z0-9 ]', '', regex=True)

# COMMAND ----------

# clean 'BillingCountry', 'BillingCity', 'BillingState' - 2, 'BillingPostalCode' - 1373
OG_Salesforce_Account[OG_Salesforce_Account['BillingState'].isnull()]

# COMMAND ----------

# clean 'BillingCountry', 'BillingCity', 'BillingState' - 2, 'BillingPostalCode' - 1373
OG_Salesforce_Account[OG_Salesforce_Account['BillingState'].isnull()]

# COMMAND ----------

# import requests
# import json
# import re

# def cleaning_city(row, current, key):

#     if pd.isnull(current):
#         print(current)
#         print('https://nominatim.openstreetmap.org/search?q='+row['BillingPostalCode']+' '+row['BillingCountry']+'&format=json&polygon=1&addressdetails=1')
#         receive = requests.get('https://nominatim.openstreetmap.org/search?q='+row['BillingPostalCode']+' '+row['BillingCountry']+'&format=json&polygon=1&addressdetails=1',verify=False)
#         info = json.loads(receive.content.decode('utf-8'))
# #         print(info)
#         if len(info) > 0:
#             info = info[0]
#             print(info)
#             return info['address']['state']
#         return None
#     else:
#         if current.isupper() or current.islower(): return current.capitalize()
#         return current
        
# OG_Salesforce_Account['BillingState'] = OG_Salesforce_Account.apply(lambda row: cleaning_city(row, row['BillingState'], 'statedistrict'),1)

# COMMAND ----------

# clean 'BillingCountry', 'BillingCity', 'BillingState' - 2, 'BillingPostalCode' - 1373
# OG_Salesforce_Account[OG_Salesforce_Account['BillingState'].isnull()]

# COMMAND ----------



def cleaning_postalcode(row):
    location_search = ''
    if pd.notnull(row['BillingStreet']):
        street= row['BillingStreet'].replace('District', '').replace('Unit', '').replace(' Lane ', ' ').replace('No.', '').replace('NO.', '').replace('No ', '').replace('\r\n', ',').replace('PO Box', '').replace('#', '')

        temp_street = None
        if ',' in street:
            listz = street.split(',')
            for value in ['Road', 'Street', 'Ave', 'Blvd', 'Rd', 'St.', 'Way']:
                has_number = None
                for y in range(0,len(listz)):        
                    if re.search(value, listz[y], re.IGNORECASE):
                        temp_street = listz[y]

                        if has_number != None and temp_street != has_number and bool(re.search(r'\d', temp_street)) == False:
                            temp_street = has_number +' '+ temp_street

                        break

                    if bool(re.search(r'\d', listz[y])): 
                        has_number = listz[y].replace('P.O. Box', '')

                if temp_street != None: 
                    street = temp_street
                    break
        location_search = street
    if pd.notnull(row['BillingCity']): location_search = location_search + " " + row['BillingCity']
    if pd.notnull(row['BillingState']):location_search = location_search + " " + row['BillingState']
    if pd.notnull(row['BillingPostalCode']):location_search = location_search + " " + row['BillingPostalCode']
    if pd.notnull(row['BillingCountry']):location_search = location_search + " " + row['BillingCountry']
    print(location_search)
    receive = requests.get('https://nominatim.openstreetmap.org/search?q='+location_search+'&format=json&polygon=1&addressdetails=1',verify=False)
    info = receive.content.decode('utf-8')
    print(info)
    print(type(info))
    if info != '[]':
        info = json.loads(info)
        print(info)
        print(type(info))
        info = info[0]
        if 'postcode' in info['address'].keys(): return info['address']['postcode']
    return None
        
    
# OG_Salesforce_Account['BillingPostalCode'] = OG_Salesforce_Account.apply(lambda row: row['BillingPostalCode'] if pd.notnull(row['BillingPostalCode']) else( row['DB_Postal_Code__c'] if pd.notnull(row['DB_Postal_Code__c'])  else cleaning_postalcode(row)), 1)

# COMMAND ----------

DNBoptimizer_mail_cols = [x for x in OG_Salesforce_Account.columns if 'DNBoptimizer__Mailing' in x]
DNBoptimizer_mail_cols

# COMMAND ----------

OG_Salesforce_Account = OG_Salesforce_Account.drop(DNBoptimizer_mail_cols,1)

# COMMAND ----------

for x in OG_Salesforce_Account.columns:
    if 'DNBoptimizer__' in x:
        print(x)
        print(OG_Salesforce_Account[x].unique())

# COMMAND ----------

OG_Salesforce_Account.columns

# COMMAND ----------

OG_Salesforce_Account['DB_Import__c'] = OG_Salesforce_Account['DNBoptimizer__ImporterExporterDescription__c'].apply(lambda value: 1 if pd.notnull(value) and ('Import' in value) else 0 ,1)
OG_Salesforce_Account['DB_Export__c'] = OG_Salesforce_Account['DNBoptimizer__ImporterExporterDescription__c'].apply(lambda value: 1 if pd.notnull(value) and ('Export' in value) else 0,1)
OG_Salesforce_Account['DB_Agent__c'] = OG_Salesforce_Account['DNBoptimizer__ImporterExporterDescription__c'].apply(lambda value: 1 if pd.notnull(value) and ('Agent' in value) else 0,1)

# COMMAND ----------

OG_Salesforce_Account['DNBoptimizer__TelephoneNumber__c'] = OG_Salesforce_Account['DNBoptimizer__TelephoneNumber__c'].apply(lambda value: 1 if pd.notnull(value) else 0 ,1)
OG_Salesforce_Account['DNBoptimizer__WebAddress__c'] = OG_Salesforce_Account['DNBoptimizer__WebAddress__c'].apply(lambda value: 1 if pd.notnull(value) else 0 ,1)
OG_Salesforce_Account['Phone'] = OG_Salesforce_Account['Phone'].apply(lambda value: 1 if pd.notnull(value) else 0 ,1)
OG_Salesforce_Account['Website'] = OG_Salesforce_Account['Website'].apply(lambda value: 1 if pd.notnull(value) else 0 ,1)
OG_Salesforce_Account['Forms of Interaction'] = OG_Salesforce_Account['DNBoptimizer__TelephoneNumber__c']+OG_Salesforce_Account['DNBoptimizer__WebAddress__c']+OG_Salesforce_Account['Phone']+OG_Salesforce_Account['Website']

# COMMAND ----------

t_f_cols = ['C4C_Enabled__c',  'KimbleOne__IsCustomer__c']

for col in t_f_cols:
    OG_Salesforce_Account[col] = OG_Salesforce_Account[col].replace({True:1, False: 0})

# COMMAND ----------

OG_Salesforce_Account = OG_Salesforce_Account.drop(['DNBoptimizer__ImporterExporterDescription__c','DNBoptimizer__TelephoneNumber__c', 'DNBoptimizer__WebAddress__c', 'Phone', 'Website'], 1)

# COMMAND ----------

OG_Salesforce_Account['LastActivityDate'] = pd.to_datetime(OG_Salesforce_Account['LastActivityDate']).dt.tz_localize(None)
OG_Salesforce_Account['CreatedDate'] = pd.to_datetime(OG_Salesforce_Account['CreatedDate']).dt.tz_localize(None)

# COMMAND ----------

from datetime import datetime

todayz = datetime.now()
print(todayz)
OG_Salesforce_Account['Days Since Last Active'] = OG_Salesforce_Account.apply(lambda row: (todayz-row['LastActivityDate']).days if pd.notnull(row['LastActivityDate']) else None ,1)

# COMMAND ----------

OG_Salesforce_Account.info()

# COMMAND ----------

date_time_cols = ['Master_Contract_Anniversary_Date__c', 'Master_Contract_Change_Date__c', 'Master_Contract_Contract_Start_Date__c', 'Master_Contract_CreatedDate', ]
for col in date_time_cols:
    OG_Salesforce_MasterContract[col] = pd.to_datetime(OG_Salesforce_MasterContract[col]).dt.tz_localize(None)

# COMMAND ----------

# compare Account id to Quote_BillTo__c
# Quote_Competitors__c split
# Quote_RecordTypeId label 1,2,3
t_f_colz =['Quote_Special_Inspection_Quote__c', 'Quote_QuoteLocked__c','Quote_Quote_Locked_2__c','Quote_Quote_Accepted_Not_Won__c', 'Quote_Projected_Delivery_Date_Exists__c','Quote_Original_QL_IFE__c', 'Quote_Original_QL_Admin__c','Quote_Net_Amount_Greater_Zero_Output__c','Quote_Model_Exist_Output__c', 'Quote_Locked__c', 'Quote_IssuedLastYear__c', 'Quote_Factory_Exists__c', 'Quote_Exchange_Rate_Applied__c', 'Quote_CustomerBillingBlock__c', 'Quote_Agent_Exists_Output__c', 'Quote_Accredited_Services_Exist_Output__c', 'Quote_Bypass__c', 'Quote_Bypass_Expiration__c']

for col in t_f_colz:
    OG_Salesforce_Quote[col] = OG_Salesforce_Quote[col].replace({True:1, False: 0})

# COMMAND ----------

# MAGIC %md
# MAGIC # So Accounts can link to Quotes or Account MC can link to Quote

# COMMAND ----------

Accounts_with_MC = pd.merge(OG_Salesforce_Account, OG_Salesforce_MasterContract, how='left', left_on='Id', right_on='Master_Contract_Customer__c').drop('Master_Contract_Customer__c',1)
Accounts_with_MC.info()

# COMMAND ----------

# MAGIC %md
# MAGIC # Linking Quote to Account with MC 

# COMMAND ----------

All_Info = pd.merge(Accounts_with_MC, OG_Salesforce_Quote, how='inner', left_on=['Id'], right_on=['Quote_SBQQ__Account__c']).drop(['Quote_SBQQ__Account__c', 'Quote_MasterContractCustom__c'],1)

# COMMAND ----------

All_Info.sort_values(by=['Id', 'Quote_CreatedDate'])[['Id','CreatedDate', 'Quote_CreatedDate', 'Quote_Id','Quote_Name','Quote_SBQQ__Status__c','Master_Contract_CreatedDate', 'Master_Contract_Id']]

# COMMAND ----------

All_Info = All_Info[All_Info['Quote_SBQQ__Status__c'] == 'Quote Accepted by Customer'].sort_values(by=['Id', 'Quote_CreatedDate'])
All_Info

# COMMAND ----------

All_Info['Quote_SBQQ__Status__c'].unique()

# COMMAND ----------

All_Info[All_Info["Name"] == "Luxx Lighting Co."][['Id', 'Quote_CreatedDate','Quote_Name']]

# COMMAND ----------

Fist_Quote_Per_Account = pd.DataFrame(All_Info.sort_values(by=['Id', 'Quote_CreatedDate']).groupby('Id').agg({'Quote_Id':'first', 'Quote_CreatedDate':'first'})).reset_index()
Fist_Quote_Per_Account['First Quote Accepted by Customer'] = 1
Fist_Quote_Per_Account['Joined FY'] = Fist_Quote_Per_Account['Quote_CreatedDate'].apply(lambda value: "This FY" if value >='2020-04-01' else("Last FY" if value >='2019-04-01' else("2 FY ago" if value >='2018-04-01' else "3 FY ago")), 1)
Fist_Quote_Per_Account

# COMMAND ----------

# Fist_Quote_Per_Account[Fist_Quote_Per_Account["Name"] == "Luxx Lighting Co."][['Id', 'Quote_CreatedDate','Quote_Name']]

# COMMAND ----------

Fist_Quote_Per_Account.groupby('Id')['Quote_Id'].count()

# COMMAND ----------

New_Customers = pd.merge(All_Info, Fist_Quote_Per_Account[['Id','Quote_Id','First Quote Accepted by Customer']], how='left', on=['Id', 'Quote_Id'])
New_Customers['First Quote Accepted by Customer'].fillna(0, inplace=True)
New_Customers

# COMMAND ----------

New_Customers = pd.merge(New_Customers, Fist_Quote_Per_Account[['Id','Quote_CreatedDate','Joined FY']].rename(columns={'Quote_CreatedDate': 'Quote_FirstCreatedDate'}), how='left', on=['Id'])
New_Customers

# COMMAND ----------

New_Customers.to_excel(r'C:\Users\azelisko\Documents\Power BI Demos\Farukh - Client Accquistion\Python Output - New Customers Quote Level.xlsx', sheet_name = 'Quotes', index = None, header=True)
# New_Customers.to_excel(r'C:\Users\azelisko\Documents\Power BI Demos\ELT - CSA Dashboard\Python Output - All New Customers Quote Level 2.xlsx', sheet_name = 'Quotes', index = None, header=True)

# COMMAND ----------


