# Databricks notebook source
# MAGIC %md
# MAGIC # Quote Line Resource and Cost Information
# MAGIC ### Within the quote line table we have subservice lines which relate to the project that is being booked. However the same table has line for the resources and costs that are associted to that project.In the table below we will create one line for the projects to understand what resources and cost were originally booked for the project.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Column_Transformations">Transforming Data Columns</a><br>
# MAGIC <a href="#Engineering_New_Features">Engineering New Features</a><br> 
# MAGIC <a href="#Outputing_Table">Outputing Quote Line Resiurce and Cost Feature Table</a><br> 

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = ''
for x in range(0,dir_to_data):
    string += '..\\'
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import numpy as np
import pandas as pd

from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL
from ipynb.fs.full.Column_Transformations import converting_currency_to_CAD

import Configuration_File

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote Line data from SQL database table. Subservice is the quote line relating to the project so we want to filter out those rows.

# COMMAND ----------

cols_for_QuoteLine_ls = ['Quote Line Record ID', 'Quote Line Required By','Quote Line Package Total','Quote Line Currency',\
                      'Quote Line Product Code','Quote Line Product Family']
where_clause_str = ' WHERE "Quote Line Product Family" <>' +"'Subservice'"
quoteline_info_df = GET_from_Centra_SQL(Configuration_File.QUOTELINE, cols_for_QuoteLine_ls, where_clause_str)

# COMMAND ----------

quoteline_info_df

# COMMAND ----------

quoteline_info_df.info()

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Column_Transformations'></a>
# MAGIC 
# MAGIC ## Column Transformation
# MAGIC 
# MAGIC ### Removing columns with no information, only one value, duplicates or columns which use are intended for user or developers purposes. i.e. output to the screen, errors. Engineering a simpler column to group by.

# COMMAND ----------

# General Adminitstrative work has values with < 100 rows so we will just group those records under one Admin or General
# Dejan said most of these are administrative
mapping_general_adminstion_ls = {"AdditionalNewCertificate":"General",
"AgreementChanges":"Administration",
"CombinedServicePackage": "General",
"CompletenessCheck":"Administration",
"DuplicationofFile": "Administration",
"IgnitionHazardAssessmentCheck": "Administration",
"LabelLicensing":"General",
"NewCertificate":"General",
"NewCertificateLevy":"General",
"PrintingCharge": "Administration",
"Surchargeformanufacturerlocatedinnon-IECExMembercountry.(SUR)":"General",
"StorageofAmendmentFile":"File Storage",
"StorageofTechnicalFiles(1-5)":"File Storage",
"StorageofTechnicalFiles(6-10)":"File Storage",
"L10Levy+IECExManagementCommitteeLevy-Prime(L10-P+MC-P)":"Management Committee Levy",
"L10Levy+IECExManagementCommitteeLevy-Variation(L10-V+MC-V)":"Management Committee Levy",
"DocumentsRequestedOutsideofProject":"Documents Outside of Project",
"DocumentsRequestedOutsideofProject-LegalInvolved": "Documents Outside of Project",
"ChangestoCertificateorIncrementalVariations": "Changes to Certificate",
"ChangestoCertificates": "Changes to Certificate"}

# COMMAND ----------

quoteline_info_df['Quote Line Product Code'] = quoteline_info_df['Quote Line Product Code']\
                                                .replace(mapping_general_adminstion_ls)

# COMMAND ----------

quoteline_info_df

# COMMAND ----------

quoteline_info_df['Overall Item'] = quoteline_info_df['Quote Line Product Family'] + " "\
                                    + quoteline_info_df['Quote Line Product Code']

# COMMAND ----------

# required by is assigned as the quote line the resource or cost rolls up to
quoteline_info_df['Quote Line Record ID'] = quoteline_info_df.apply(lambda row:row['Quote Line Required By'] 
                                                                               if pd.notnull(row['Quote Line Required By']) 
                                                                               else row['Quote Line Record ID'],1 )

# COMMAND ----------

quoteline_info_df

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Engineering_New_Features'></a>
# MAGIC 
# MAGIC ## Engineering New Features
# MAGIC ### Creating new features based on exsisting columns

# COMMAND ----------

# that package total isnt converted into canadian currency
quoteline_info_df = converting_currency_to_CAD(quoteline_info_df, ['Quote Line Package Total'], 'Quote Line Currency')

# COMMAND ----------

quoteline_info_df = quoteline_info_df.drop(['Quote Line Product Code', 'CurrencyType Conversion Rate',
                                         'Quote Line Package Total', 'Quote Line Currency', 'Quote Line Required By'],1)

# COMMAND ----------

quoteline_base_df = pd.DataFrame()

# pivot each of the string columns to create additional features
for level in ["Quote Line Product Family"]:
    pivotby = pd.pivot_table(quoteline_info_df, values=["Quote Line Package Total (CAD)"], index=["Quote Line Record ID"],
                             columns =[level], aggfunc={"Quote Line Package Total (CAD)":[np.sum, "count"]},
                             fill_value=0).reset_index()
    
    new_columns = ['{}_{}_{}'.format(col, function, detail.title())\
                   .replace('Quote Line Package Total (CAD)_count_', "Number of ")\
                   .replace("_sum_", " ") for col, function, detail, in pivotby.columns]
    new_columns.remove("Quote Line Record ID__")
    new_columns = ["Quote Line Record ID"] + new_columns

    pivotby.columns = new_columns

    if quoteline_base_df.empty:
        quoteline_base_df = pivotby
    else:
        quoteline_base_df = pd.merge(quoteline_base_df, pivotby, how="inner", on="Quote Line Record ID")

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Outputing_Table'></a>
# MAGIC 
# MAGIC ## Outputting Table
# MAGIC ### Exporting the table 

# COMMAND ----------

quoteline_base_df.info()

# COMMAND ----------

quoteline_base_df.to_csv(Configuration_File.QUOTELINES_RESOURCE_COST_DETAILS, index = None, header=True)

# COMMAND ----------


