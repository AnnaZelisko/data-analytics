# Databricks notebook source
# MAGIC %md
# MAGIC # Project Days in Milestone
# MAGIC ### Table informs us of the number of days that a project has been in a specific milestone, along with additional features just as total days on hold. Total days on hold before project was started. etc. 
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Fill_Missing_Tasks">Fill Missing Tasks Using Template Mapping</a><br>
# MAGIC <a href="#Adding_Project_Info">Adding Project Info</a><br>
# MAGIC <a href="#Quote_Accept_Data_Quality">Removing Projects with Quote_Accept Data Quality</a><br>
# MAGIC <a href="#Setting_Up_for_Difference">Configuring Dataframe To Get Date Difference</a><br>
# MAGIC <a href="#Integrating_Hold_Days">Integrating Hold Days</a><br>
# MAGIC <a href="#Creating_Final_Table">Creating & Saving Final Table</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = "..\\" * dir_to_data
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

from string import ascii_letters
import numpy as np
import pandas as pd
import seaborn as sns 

from ipynb.fs.full.Getting_Data import GET_from_Excel, GET_from_Centra_SQL

import Configuration_File

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Getting the Project Milestone, Project Template and Project data together to analysis the duration and associated hold days.

# COMMAND ----------

milestone_col_ls = ["Project Milestone Completed Date", "Project Milestone Project Detail","Project Milestone Task Name",
                    "Project Milestone Milestone Name"]
where_clause_str = " WHERE [Project Milestone Completed Date] IS NOT NULL"
project_milestone_base_df  = GET_from_Centra_SQL(Configuration_File.PROJECTMILESTONE, milestone_col_ls, where_clause_str)

# COMMAND ----------

# filter based on date - for this fiscal year
project_milestone_base_df = project_milestone_base_df[project_milestone_base_df['Project Milestone Completed Date']
                                                      >=  "2019-04-01"]

# COMMAND ----------

# Task and Milestone Mapping as we are missing 848-647 = 201 tasks
milestone_col_ls = ["Milestone_Name__c", "Task_Name__c"]
task_mapping_df  = GET_from_Centra_SQL(Configuration_File.MILESTONETEMPLATE, milestone_col_ls).drop_duplicates()

# COMMAND ----------

# left join to keep all the milestone projects
duplicate_project_milestone_df = pd.merge(project_milestone_base_df, task_mapping_df, how='left', 
                                       left_on=['Project Milestone Milestone Name'], right_on=['Milestone_Name__c'])

# COMMAND ----------

duplicate_project_milestone_df.info()

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Fill_Missing_Tasks'></a>
# MAGIC  
# MAGIC ## Handling Missing Task Names
# MAGIC ### Found that some of our projects are missing task names, we can fill them in based on the left join. As we have more milestones than task names.

# COMMAND ----------

# Filling in the missing up task name
duplicate_project_milestone_df['Task Name'] = duplicate_project_milestone_df.apply(lambda row: 
                                                                    row['Project Milestone Task Name'] 
                                                                    if pd.notnull(row['Project Milestone Task Name']) 
                                                                    else row['Task_Name__c'], axis=1)

no_longer_needed_ls = ['Project Milestone Milestone Name', 'Project Milestone Task Name', 'Milestone_Name__c', 
                       'Task_Name__c' ]
duplicate_project_milestone_df = duplicate_project_milestone_df.drop(no_longer_needed_ls, 1)

# COMMAND ----------

# grabbing the lastest task date
clean_project_milestone_df = duplicate_project_milestone_df.groupby(["Project Milestone Project Detail","Task Name"])\
                                ['Project Milestone Completed Date'].max().reset_index()\
                                .rename(columns={'max':'Project Milestone Completed Date', 
                                                 'Task Name':'Project Milestone Task Name'})

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Adding_Project_Info'></a>
# MAGIC  
# MAGIC ## Incorporating Project Information
# MAGIC ### We need the project quote accepted date.

# COMMAND ----------

# main Project Table to get the quote accepted date 
project_col_ls = ["Project Detail Project Status Reporting", "Project Detail Record ID","Project Detail Name",
                    "Project Detail Quote Accepted Date"]
where_clause_str = " WHERE [Project Detail Project Status Reporting] = 'Closed - Complete' AND [Project Detail Quote Accepted Date] IS NOT NULL"
main_project_detail_df  = GET_from_Centra_SQL(Configuration_File.PROJECTDETAIL, project_col_ls, where_clause_str)

# COMMAND ----------

main_project_detail_df['Change Order'] = main_project_detail_df.apply(lambda row: 1 
                                                                      if 'CO0' in row['Project Detail Name'] 
                                                                      or 'Change Order' in row['Project Detail Name'] 
                                                                      else 0, 1 )

# COMMAND ----------

# Main Project Table to get the quote accepted date 
# main_project_detail_df  = GET_from_Centra_SQL(Configuration_File.PROJECTDETAIL, project_col_ls)

# COMMAND ----------

# inner join because we want all the projects that have a quote accepted date
closed_project_wmilestones_df = pd.merge(clean_project_milestone_df, main_project_detail_df,
                                     left_on=['Project Milestone Project Detail'], 
                                     right_on = ['Project Detail Record ID']).drop(['Project Detail Record ID'],1)

# COMMAND ----------

# converting columns to datetime
col_to_dt_ls = ['Project Milestone Completed Date', 'Project Detail Quote Accepted Date']
for col in col_to_dt_ls:
    closed_project_wmilestones_df[col] = pd.to_datetime(closed_project_wmilestones_df[col])

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Quote_Accept_Data_Quality'></a>
# MAGIC  
# MAGIC ## Data Quality: Quote Accept
# MAGIC ### Some of the project milestone are before the quote accepted date. We will exclude those projects from further investigation.

# COMMAND ----------

# Data quality some projects have milestone dates before the quote was accepted which should be possible
bad_projects_ls = closed_project_wmilestones_df.loc[closed_project_wmilestones_df['Project Milestone Completed Date'] 
                                                 <= (closed_project_wmilestones_df['Project Detail Quote Accepted Date']),
                                                          "Project Milestone Project Detail"].unique()

# COMMAND ----------

len(set(bad_projects_ls))

# COMMAND ----------

# Drop projects with that detail
closed_project_wmilestones_df = closed_project_wmilestones_df[
    ~closed_project_wmilestones_df['Project Milestone Project Detail'].isin(bad_projects_ls)]

# COMMAND ----------

closed_project_wmilestones_df

# COMMAND ----------

# MAGIC %md
# MAGIC  <a id='Setting_Up_for_Difference'></a>
# MAGIC  
# MAGIC ## Setting Up Table to Get the Date Diff
# MAGIC ### Quote Accepted is a column it needs to be configured to be a row.

# COMMAND ----------

# creating a pivot table so that Quote Accepted Date becomes a row rather than a column
pivot_quote_accept_df = closed_project_wmilestones_df.drop(['Project Milestone Completed Date',
                                                         'Project Milestone Task Name'],1).drop_duplicates()
pivot_quote_accept_df['Project Milestone Task Name'] = '1.Quote Accepted'
pivot_quote_accept_df['Project Milestone Completed Date'] = pd.to_datetime(pivot_quote_accept_df['Project Detail Quote Accepted Date'].astype(str)+' 00:00:00')
pivot_quote_accept_df.head(5)

# COMMAND ----------

# unioning the quote accepted stuff
project_milestones_with_quote_accept_df = closed_project_wmilestones_df.append(pivot_quote_accept_df, ignore_index = True)\
                                        .drop(['Project Detail Quote Accepted Date'],1)

# COMMAND ----------

# sorting of values for diff
project_milestones_with_quote_accept_df.sort_values(by=['Project Milestone Project Detail',
                                                     'Project Milestone Completed Date', 
                                                     'Project Milestone Task Name'], inplace=True)

# COMMAND ----------

# this is the row difference function
project_milestones_with_quote_accept_df['Date Diff'] = project_milestones_with_quote_accept_df\
                                                    .groupby('Project Milestone Project Detail')\
                                                    ['Project Milestone Completed Date'].diff()

# COMMAND ----------

# sicne quote accepted will always have NAT
project_milestones_all_task_days_df = project_milestones_with_quote_accept_df[project_milestones_with_quote_accept_df['Project Milestone Task Name']!='1.Quote Accepted']
project_milestones_all_task_days_df.info()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Integrating_Hold_Days'></a>
# MAGIC  
# MAGIC ## Integrating Hold Days
# MAGIC ### Getting hold days and determining wether the hold days fall into the duration of a speific milestone task.

# COMMAND ----------

# Project Hold table
project_hold_col_ls = ["Project Detail Hold Centra Project Detail", "Project Detail Hold Date On Hold", 
                  "Project Detail Hold Date Off Hold", "Project Detail Hold Days on Hold"]
where_clause_str = " WHERE [Project Detail Hold Date Off Hold] IS NOT NULL"
project_hold_df  = GET_from_Centra_SQL(Configuration_File.PROJECTDETAILHOLD, project_hold_col_ls, where_clause_str)

# COMMAND ----------

# converting columns to datetime
col_to_dt_ls = ['Project Detail Hold Date On Hold', 'Project Detail Hold Date Off Hold']
for col in col_to_dt_ls:
    project_hold_df[col] = pd.to_datetime(project_hold_df[col])

# COMMAND ----------



# COMMAND ----------

# all the projects on hold
subset_hold_project_task_df = pd.merge(project_milestones_all_task_days_df, project_hold_df, 
                                    left_on=['Project Milestone Project Detail'], 
                                    right_on = ['Project Detail Hold Centra Project Detail'])
subset_hold_project_task_df.info()

# COMMAND ----------

subset_hold_project_task_df.sort_values(['Project Milestone Project Detail','Project Milestone Completed Date'])

# COMMAND ----------

subset_hold_project_task_df['Hold Days'] = np.nan

def checking_hold(row):
    start_milestone_date = row['Project Milestone Completed Date'] - row['Date Diff']
    if (start_milestone_date <= row['Project Detail Hold Date On Hold'])\
        & (row['Project Detail Hold Date Off Hold'] <= row['Project Milestone Completed Date']):
        return pd.Series(row['Project Detail Hold Days on Hold'])
    return pd.Series(0)
    
subset_hold_project_task_df['Hold Days'] = subset_hold_project_task_df.apply(lambda row: checking_hold(row), axis=1)

# COMMAND ----------

subset_hold_project_task_df

# COMMAND ----------

# drop all the recods where the hold didnt happen there
subset_hold_project_task_df = subset_hold_project_task_df.dropna(subset=['Hold Days'])
subset_hold_project_task_df.info()

# COMMAND ----------

# certian projects have multiple holds in the same task.... so we have to add all the hold days up together! 
# ex. a9T1L0000008OVHUA2 task 7. Conduct Project we have a record of hold  being 75 days and then being 2 days
subset_hold_project_task_agg_df = subset_hold_project_task_df.groupby(by=['Project Detail Project Status Reporting', 
                                                                          'Project Milestone Completed Date', 
                                                                          'Project Milestone Project Detail', 
                                                                          'Project Milestone Task Name', 'Date Diff'])\
                                                                ['Hold Days'].sum().reset_index()

# COMMAND ----------

# cleaning up shop
subset_hold_project_task_agg_df = subset_hold_project_task_agg_df.drop(['Project Detail Project Status Reporting',
                                                                        'Project Milestone Completed Date', 
                                                                        'Date Diff'],1).drop_duplicates()

# COMMAND ----------

subset_hold_project_task_agg_df.info()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Creating_Final_Table'></a>
# MAGIC 
# MAGIC ## Creating Final Table
# MAGIC ### Putting together and saving the final table

# COMMAND ----------

# now we need to merge the hold projects with the original milestone table based on id and Project Milestone task
final_project_task_df = pd.merge(project_milestones_all_task_days_df, subset_hold_project_task_agg_df, how='left', 
                                 on=['Project Milestone Project Detail', 'Project Milestone Task Name'])
final_project_task_df['Hold Days'].fillna(0, inplace=True)
final_project_task_df

# COMMAND ----------

#subtracting the total hold days from task days
final_project_task_df['Total Task Days'] = final_project_task_df['Date Diff'].dt.days - final_project_task_df['Hold Days']

# COMMAND ----------

final_project_task_df[final_project_task_df['Total Task Days'] > 1000]

# COMMAND ----------

final_project_task_df.to_excel(Configuration_File.PROJECT_TASK_DAYS, index = None, header=True)

# COMMAND ----------


