# Databricks notebook source
# MAGIC %md
# MAGIC # Quote Stage Duration Table
# MAGIC ### Each quote resides in a quote stage for a certain period of time. Quote stages and Opportunity stages map together. This table will allow us to understand for how long a quote has been in a specific stage.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Data_Transformation">Data Transformation</a><br>
# MAGIC <a href="#Quote_Stages_Data_Quality_Records">Quote Stages with Data Quality Records</a><br>
# MAGIC <a href="#Quote_Stage_Difference">Quote Stage Difference</a><br>
# MAGIC <a href="#Engineering_New_Features">Engineering New Features</a><br> 
# MAGIC <a href="#Outputing_Table">Outputing Quote Stage Table</a><br> 

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = ''
for x in range(0,dir_to_data):
    string += '..\\'
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import numpy as np
import pandas as pd

from ipynb.fs.full.Getting_Data import GET_from_Centra_SQL

import Configuration_File

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Accessing Quote Line data from SQL database table. In this case we are trying to see the number of days in each stage so it makes sense to only pull the relevant quote status adn created date information.

# COMMAND ----------

cols_for_Quote_History_ls = ['Quote History Custom Object Record', 'Quote History Created Date', 'Quote History Old Value',
                             'Quote History New Value']
where_clause_str = ' WHERE "Quote History Changed Field" =' +"'SBQQ__Status__c'"\
                    + ' OR "Quote History Changed Field" =' +"'created'"
quote_status_history_df = GET_from_Centra_SQL(Configuration_File.QUOTEHISTORY, cols_for_Quote_History_ls, where_clause_str)

# COMMAND ----------

quote_status_history_df

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Data_Transformation'></a>
# MAGIC 
# MAGIC ## Data Transformation 
# MAGIC ### Creating and transforming the exsisting column. Change Field created has no new or old values so we need to fill it in. Also transforming from a string to a number makes it easier to understand what stage we are at. Also strings for stages have changed over the years so we can use numbers to bring different strings together.

# COMMAND ----------

quote_status_history_df['Quote History Old Value'] = quote_status_history_df['Quote History Old Value'].fillna('Created')
quote_status_history_df['Quote History New Value'] = quote_status_history_df['Quote History New Value'].fillna('Draft')

# COMMAND ----------

status_ls = ['Created', 'Draft', 'Pending Approval','Quote Approved', 'Quote Issued', 'Verbal Commitment', 
             'Quote Accepted by Customer', 'Quote Accepted','Quote Rejected by Customer', 'Quote Cancelled', 
             'Quote Rejected by Customer (Lost)']
stage_ls = [0,1, 2, 3, 4, 4, 5, 5, 6, 6, 6]
status_to_stages_dic = dict(zip(status_ls, stage_ls))

# COMMAND ----------

status_to_stages_dic

# COMMAND ----------

quote_status_history_df['Enumarate Quote History Old Value'] = quote_status_history_df['Quote History Old Value']\
                                                               .replace(status_to_stages_dic)
quote_status_history_df['Enumarate Quote History New Value'] = quote_status_history_df['Quote History New Value']\
                                                               .replace(status_to_stages_dic)

# COMMAND ----------

quote_status_history_df['Status Change'] = quote_status_history_df['Quote History Old Value'] + " to " +\
                                           quote_status_history_df['Quote History New Value']

# COMMAND ----------

# Verbal Commitments can be turn into a feature column 
quotes_with_verbal_commitment_ls = quote_status_history_df[quote_status_history_df['Quote History New Value']
                                                           =='Verbal Commitment']['Quote History Custom Object Record'].unique()

# COMMAND ----------

# dropping row that contain Verbal Commitments
quote_status_transformed_df = quote_status_history_df[~(quote_status_history_df['Status Change'].str\
                                                        .contains('Verbal Commitment'))]

# COMMAND ----------

quote_status_transformed_df['Stage Difference'] = quote_status_transformed_df['Enumarate Quote History New Value']\
                                                  - quote_status_transformed_df['Enumarate Quote History Old Value'] 

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Quote_Stages_Data_Quality_Records'></a>
# MAGIC 
# MAGIC ## Quote Stages Data Quality Records
# MAGIC ### Some quotes skip stages, others have stage rejected and accepted when a quote should really be either accepted or rejected not both. Some stages repeat.

# COMMAND ----------

max_stage_df = quote_status_transformed_df.groupby('Quote History Custom Object Record')['Enumarate Quote History New Value']\
                .max().reset_index().rename(columns={'Enumarate Quote History New Value': 'Max Quote Stage'})
max_stage_df

# COMMAND ----------

quote_status_transformed_df = pd.merge(quote_status_transformed_df, max_stage_df, how='left', 
                                       on='Quote History Custom Object Record')

# COMMAND ----------

quote_status_transformed_df

# COMMAND ----------

# MAGIC %md

# COMMAND ----------

# remove anything below 0
quote_status_no_reverse = quote_status_transformed_df[quote_status_transformed_df['Stage Difference'] > 0]

# COMMAND ----------

quote_status_no_reverse[quote_status_no_reverse['Quote History Custom Object Record']=='a0l4100000EAisjAAD']

# COMMAND ----------

quote_status_normal_ordering_df = quote_status_no_reverse[(quote_status_no_reverse['Stage Difference'] == 1)\
                                                       | (quote_status_no_reverse['Max Quote Stage'] == 6)]
quote_status_normal_ordering_df

# COMMAND ----------

grouping_here_ls = ["Quote History Custom Object Record",'Status Change', 'Enumarate Quote History Old Value',
                 'Enumarate Quote History New Value']
quote_status_normal_ordering_df = quote_status_normal_ordering_df.groupby(grouping_here_ls)['Quote History Created Date']\
                                                        .agg({max, 'count'}).reset_index()\
                                                        .rename(columns={'max':'Quote Field History Created Date'})

# COMMAND ----------

quote_status_normal_ordering_df

# COMMAND ----------



# COMMAND ----------

# checking general ordering
quote_status_list_df = quote_status_normal_ordering_df\
                       .sort_values(by=['Quote History Custom Object Record','Quote Field History Created Date', 
                                        'Status Change'])\
                       .groupby(['Quote History Custom Object Record'])\
                       .agg({'Quote Field History Created Date':list,'Status Change':list,
                             'Enumarate Quote History Old Value':list, 'Enumarate Quote History New Value':list})\
                       .reset_index()
quote_status_list_df

# COMMAND ----------

possible_starting_stages = [[0],[0,1],[0,1,2], [0,1,2,3], [0, 1,2,3,4]]
quote_status_list_df['Order Correct'] = quote_status_list_df['Enumarate Quote History Old Value']\
                                        .apply(lambda value:1 if value in possible_starting_stages else 0,1)

# COMMAND ----------

quote_status_list_df.groupby(['Order Correct']).count()

# COMMAND ----------

quotes_with_accurate_stages_ls = quote_status_list_df.loc[quote_status_list_df['Order Correct']==1]['Quote History Custom Object Record'].unique()

# COMMAND ----------

quotes_with_accurate_stages_ls

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Quote_Stage_Difference'></a>
# MAGIC 
# MAGIC ## Quote Stage Difference
# MAGIC ### Calculating the difference in days between stages.

# COMMAND ----------

quote_status_normal_ordering_df = quote_status_normal_ordering_df\
                                                .loc[quote_status_normal_ordering_df['Quote History Custom Object Record']\
                                                     .isin(quotes_with_accurate_stages_ls)]

# COMMAND ----------

quote_status_normal_ordering_df.sort_values(by=['Quote History Custom Object Record','Quote Field History Created Date',
                                                'Status Change'])

# COMMAND ----------

# this is the row difference function
quote_status_normal_ordering_df['Stage Date Diff'] = quote_status_normal_ordering_df\
                                                    .groupby('Quote History Custom Object Record')\
                                                    ['Quote Field History Created Date'].diff()

# COMMAND ----------

quote_status_normal_ordering_df

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Engineering_New_Features'></a>
# MAGIC 
# MAGIC ## Feature Engineering
# MAGIC ### Pivoting the table from rows to columns so we can see the quote stages as a column view.

# COMMAND ----------

quotes_with_stages_days_df = pd.pivot_table(quote_status_normal_ordering_df, values='Stage Date Diff',
                                            index=['Quote History Custom Object Record'],
                                            columns=['Enumarate Quote History Old Value'],
                                            aggfunc=np.sum)\
                               .reset_index()
quotes_with_stages_days_df.columns = ['Quote History Custom Object Record','Created','Draft','Pending Approval',
                                      'Quote Approved','Quote Issued']

# COMMAND ----------

quotes_with_stages_days_df.describe()

# COMMAND ----------

quotes_with_stages_days_df = quotes_with_stages_days_df.drop('Created',1)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Outputing_Table'></a>
# MAGIC 
# MAGIC ## Final Output Table
# MAGIC ### Quote History feature relate to the quote so they will join on the quote base table.

# COMMAND ----------

quote_stages_final_df = pd.merge(quote_status_history_df[['Quote History Custom Object Record']].drop_duplicates(), 
                              quotes_with_stages_days_df,how='left', on='Quote History Custom Object Record')

# COMMAND ----------

quote_stages_final_df['Verbal Commitment'] = quote_stages_final_df['Quote History Custom Object Record']\
                                                 .apply(lambda idz:1 if idz in quotes_with_verbal_commitment_ls else 0,1)

# COMMAND ----------

quote_stages_final_df = quote_stages_final_df.rename(columns={'Quote History Custom Object Record':'Quote Record ID'})

# COMMAND ----------

quote_stages_final_df.info()

# COMMAND ----------

quote_stages_final_df.to_csv(Configuration_File.QUOTES_STAGE_DAYS, index = None, header=True)

# COMMAND ----------


