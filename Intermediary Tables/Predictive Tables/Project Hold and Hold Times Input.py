# Databricks notebook source
# MAGIC %md
# MAGIC # Project Hold Time Input 
# MAGIC ### Creating the general input table for the project start delay model.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Transforming_Columns">Transforming Columns</a><br>
# MAGIC <a href="#Grouping_Quote_Lines_by_Quote">Grouping Quote Lines by Quote</a><br>
# MAGIC <a href="#Splitting_Table">Splitting Table</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = "..\\" * dir_to_data
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import pandas as pd
import numpy as np
import datetime as datetime
import seaborn as sns


from ipynb.fs.full.Getting_Data import GET_from_Excel, GET_from_Centra_SQL
from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns, flagging_id_columns,flagging_columns_high_correlation
from ipynb.fs.full.Column_Transformations import transforming_tf_to_binary_columns, engineering_date_features

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Data from New bookings will drive the model as the report informs us of which quotes have been accepted and rejected.
# MAGIC ### Also immediatly joining with Quote Base table.

# COMMAND ----------

project_original_base_df = GET_from_Excel(Configuration_File.PROJECT_BASE_BUSINESS, "csv")

# COMMAND ----------

project_original_base_df = project_original_base_df.dropna(subset=["Project Detail Project Status Reporting"])

# COMMAND ----------

project_original_base_df = project_original_base_df[(project_original_base_df['Project Detail Record Type Name']\
                                                     != "Change_Order_Record_Type")\
                                  & (project_original_base_df["Project Detail Main Project Detail Name"].notnull())]

# COMMAND ----------

project_base_df = project_original_base_df[project_original_base_df["Project Detail Project Status Reporting"]\
                                           == "Closed - Complete"]

# COMMAND ----------

# this model will be run on active projects, most wont have the features that closed - completed have filled in 
project_col_filtering_df = project_original_base_df[project_original_base_df["Project Detail Project Status Reporting"]\
                                                    == "Active"]

# COMMAND ----------

project_col_filtering_df

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(project_col_filtering_df)
columns_for_inspection_dic

# COMMAND ----------

project_base_df = project_base_df.drop(list(columns_for_inspection_dic.keys()),1)

# COMMAND ----------

project_base_df[project_base_df['Project Length: Day Created to Closed'] < 0]

# COMMAND ----------

# Add to align with Adnans project List


# COMMAND ----------

#quoteline and project should be 1-1
quoteline_base_df = GET_from_Excel(Configuration_File.QUOTELINE_BASE_BUSINESS, "csv")
quoteline_base_df

# COMMAND ----------

for col in quoteline_base_df.columns:
    if "Quotelines Resource Cost Details" in col or "(CAD)" in col:
        quoteline_base_df[col] = quoteline_base_df[col].fillna(0)

# COMMAND ----------

project_base_df = pd.merge(project_base_df.drop('CurrencyType Conversion Rate',1), quoteline_base_df, 
                           how='inner', left_on = 'Project Detail Quote Line', right_on ='Quote Line Record ID')\
                       .drop(['Quote Line Record ID', 'Project Detail Quote Line', 'Project Detail Quote Name',
                              'Quote Line Line Name', 'Quote Line Currency', 'Quote Line External Scope Language',
                              'Project Detail Project GBU', 'Quote Line Project Status', 'Quote Line SAP_SO_Item',
                              'Quote Line Global Business Unit', 'Quote Line Service Delivery Location',
                              'Quote Line SAP_Service_Tracking_ID', 'Quote Line Planned Start Date',
                              'Quote Line Planned Start Date - Quote Create Date', 'Project Detail Quote Line Name',
                              'Quote Line Projected Delivery Date', 'Project Detail Product Code', 
                              'Quote Line Product Name'],1)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Transforming_Columns'></a>
# MAGIC 
# MAGIC ## Transforming Columns
# MAGIC ### Columns need to be transformed to prepare for 

# COMMAND ----------

for col in project_base_df.columns:
    if ("Date" in col or "Modstamp" in col) and project_base_df[col].dtype != "datetime64[ns]":
        project_base_df[col] = pd.to_datetime(project_base_df[col], errors='ignore')

# COMMAND ----------

ids_in_project_features_ls = flagging_id_columns(project_base_df)
ids_in_project_features_ls.remove('Project Detail Record ID')

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(project_base_df)

# COMMAND ----------

columns_for_inspection_dic

# COMMAND ----------

ids_in_project_features_ls + list(columns_for_inspection_dic.keys())

# COMMAND ----------

project_base_df = project_base_df.drop(ids_in_project_features_ls + list(columns_for_inspection_dic.keys()),1)

# COMMAND ----------

project_base_df = transforming_tf_to_binary_columns(project_base_df)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ## Engineering and Fill nans with most common values or means
# MAGIC ### Some of our column values have nulls of nans we want to fill those with the mean/min or most common occuring iteam in our column. In the future this data quality should be improved over all

# COMMAND ----------

project_base_df['Master Contract Exists']\
    = project_base_df['Project Detail Master Contract Number'].apply(lambda value: 1 if value is not None else 0,1)

# COMMAND ----------

project_base_df['Primary Contact Email Exists']\
    = project_base_df['Project Detail Primary Contact Email'].apply(lambda value: 1 if value is not None else 0,1)

# COMMAND ----------

project_base_df['PO Number Exists']\
    = project_base_df['Project Detail PO Number'].apply(lambda value: 1 if value is not None else 0,1)

# COMMAND ----------

project_base_df['Project Detail Service Tracking Number Exists']\
    = project_base_df['Project Detail Service Tracking Number'].apply(lambda value: 1 if value is not None else 0,1)

# COMMAND ----------

project_base_df['Length of Customer Product Description'] = len(project_base_df['Quote Line Customer Product Description'])

# COMMAND ----------

exists_col_ls = ['Project Detail Master Contract Number',
                 'Project Detail Primary Contact Email', 'Project Detail PO Number', 
                 'Project Detail Service Tracking Number', 'Quote Line Customer Product Description']
project_base_df = project_base_df.drop(exists_col_ls,1)

# COMMAND ----------

project_base_df.loc[project_base_df['Project Detail Planned Start Date'].isnull(),
                             'Project Detail Planned Start Date']\
    = project_base_df['Project Detail Created Date'] + datetime.timedelta(days=90)

# COMMAND ----------

project_base_df.loc[project_base_df['Project Detail Planned Delivery Date'].isnull(),
                    'Project Detail Planned Delivery Date']\
    = project_base_df['Project Detail Created Date'] + datetime.timedelta(days=90)

# COMMAND ----------

project_base_df.loc[project_base_df['Quote Line Quote Expiration Date'].isnull(),
                    'Quote Line Quote Expiration Date']\
    = project_base_df['Quote Line Created Date'] + datetime.timedelta(days=90)

# COMMAND ----------

project_base_df.loc[project_base_df['Quote Line Cust. Desired Date'].isnull(),
                    'Quote Line Cust. Desired Date']\
    = project_base_df['Quote Line Created Date'] + datetime.timedelta(days=90)

# COMMAND ----------

project_base_df['Project Detail Master Contract Status']\
    = project_base_df['Project Detail Master Contract Status'].fillna("Unknown")

# COMMAND ----------

project_base_df.loc[project_base_df['Project Detail Quote Accepted Date'].isnull(),
                             'Project Detail Quote Accepted Date'] = project_base_df['Project Detail Created Date']

# COMMAND ----------

project_base_df.loc[project_base_df['Project Detail Element Last Edited Date'].isnull(),
                             'Project Detail Element Last Edited Date'] = project_base_df['Quote Line Last Modified Date']

# COMMAND ----------

project_base_df.loc[project_base_df['Quote Line Created Date'].isnull(),
                             'Quote Line Created Date']\
    = project_base_df['Project Detail Created Date']

# COMMAND ----------

project_base_df.loc[project_base_df['Quote Line Last Modified Date'].isnull(),
                             'Quote Line Last Modified Date']\
    = project_base_df['Project Detail Created Date']

# COMMAND ----------

project_base_df.loc[project_base_df['Quote Line System Modstamp'].isnull(),
                             'Quote Line System Modstamp']\
    = project_base_df['Project Detail Created Date']

# COMMAND ----------

project_base_df['Quote Line Number'] = project_base_df['Quote Line Number'].fillna(1)

# COMMAND ----------

for x in project_base_df.columns:
    if project_base_df[x].isnull().sum() >0:
        print(x)
        print(project_base_df[x].isnull().sum())
        print(project_base_df[x].dtype)

# COMMAND ----------

hard_to_fill_ls = ['Project Detail Scope', 'Project Detail Customer Product Description', 
                   'Project Detail Service Deliver Person Email', 'Project Detail Quote Owner', 
                   'Project Detail MC No Leading Zeros', 'Project Details Complete Date Reporting',
                   'Quote Line Primary Contact Email', 'Quote Line Scope Check Completed By', 
                   'Quote Line Scope Check Completed Date', 'Quote Line Provisions']
wouldnt_provide_model_any_benefits_ls = ['Project Detail L1 Task Reporting', 'Project Detail Next Task',
                                        'Project Detail Next Task Reporting']
bias_model_ls = ['Project Detail On-Time Indicator',
                 'Project Detail Customer Project Status', 'Project Detail Project Close Date Form',
                 'Project Detail Last Completed-Task:Milestone Reporting', 'Project Detail Days Late']
fill_w_0_ls = ['Project Detail Remaining Hours/Day', 'Project Detail Remaining Usage',
              'Quote Line Additional Disc.', 'Quote Line Deposit Percentage', 'Quote Line Admin and General PF total']
fill_w_most_common_ls = ['Project Detail External Scope Language', 'Quote Line GBU',
                         'Project Detail Service Delivery Location City', 'Quote Line CU',
                         'Cross and Prodct Market Segment']
fill_w_mean = ['Project Detail Weeks to Deliver', 'Quote Line Mo from Quoted Start to Quoted Delivery',
               'Project Detail Aging from Quote Accept', 'Project Detail Accepted to Activation',
               'Project Detail Actual Hours', 
               'Project Detail Mos from Quoted Start to Quoted Delivery', 
               'Project Detail Days- Planned Closed to Planned Delivery', 
               'Project Detail Days-Planned Delivery to Planned Closed', 'Quote Line Weeks to Deliver',
               'Quote Line Weeks to Certify Rounded']

# COMMAND ----------

project_base_df = project_base_df.drop(hard_to_fill_ls + wouldnt_provide_model_any_benefits_ls + bias_model_ls,1) 
for col in fill_w_0_ls:
    project_base_df[col] = project_base_df[col].fillna(0)
    

# COMMAND ----------

# filling in based on above lists
for col in fill_w_mean:
    print(col)
    col_mean_num = project_base_df[col].mean()
    print(col_mean_num)
    project_base_df[col] = project_base_df[col].fillna(col_mean_num)

# COMMAND ----------

# filling in based on above lists
for col in fill_w_most_common_ls:
    print(col)
    col_mean_str = project_base_df[col].mode()[0]
    print(col_mean_str)
    project_base_df[col] = project_base_df[col].fillna(col_mean_str)

# COMMAND ----------

 project_base_df.loc[project_base_df['Project Detail Mos from Quoted Start to Quoted Delivery'] < 0,
                    'Project Detail Mos from Quoted Start to Quoted Delivery'] = 0

# COMMAND ----------

project_base_df

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Adding_Hold_Table'></a>
# MAGIC 
# MAGIC ## Adding in Hold Table
# MAGIC ### Adding hold information

# COMMAND ----------

# Project Hold table
project_hold_col_ls = ["Project Detail Hold Centra Project Detail", "Project Detail Hold Date On Hold", 
                      "Project Detail Hold Date Off Hold","Project Detail Hold Days on Hold"]
where_clause_str = " WHERE [Project Detail Hold Date On Hold] IS NOT NULL"
project_hold_df  = GET_from_Centra_SQL(Configuration_File.PROJECTDETAILHOLD, project_hold_col_ls, where_clause_str)

# COMMAND ----------

project_hold_df.sort_values(['Project Detail Hold Centra Project Detail','Project Detail Hold Date On Hold'])

# COMMAND ----------

project_hold_df = project_hold_df[(project_hold_df["Project Detail Hold Days on Hold"]>0)]

# COMMAND ----------

projects_with_holds_df = pd.merge(project_base_df[['Project Detail Record ID','Project Detail Created Date', 
                                                   'Project Detail Project Close Date Reporting',
                                                   'Project Length: Day Created to Closed']],
                                project_hold_df, how='inner', left_on = 'Project Detail Record ID',
                                right_on ='Project Detail Hold Centra Project Detail')\
                       .drop('Project Detail Hold Centra Project Detail',1)

# COMMAND ----------

projects_with_holds_df.info()

# COMMAND ----------

projects_with_holds_df['Project Detail Hold Date On Hold'] = pd.to_datetime(\
                                                  projects_with_holds_df['Project Detail Hold Date On Hold'], errors='ignore')

# COMMAND ----------

projects_with_holds_df['Project Detail Hold Date Off Hold'] = pd.to_datetime(\
                                                  projects_with_holds_df['Project Detail Hold Date Off Hold'], errors='ignore')

# COMMAND ----------

projects_with_holds_df['Project Detail Project Close Date Reporting'] = pd.to_datetime(\
                                                  projects_with_holds_df['Project Detail Project Close Date Reporting'], errors='ignore')

# COMMAND ----------

projects_with_holds_df[projects_with_holds_df['Project Detail Hold Date Off Hold']>projects_with_holds_df['Project Detail Project Close Date Reporting']]

# COMMAND ----------

projects_with_holds_df.info()

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Transforming_Table'></a>
# MAGIC 
# MAGIC ## Transforming Table for Distribution Analysis
# MAGIC ### Table needs to provide holds for distributions.

# COMMAND ----------

projects_with_holds_df['Days Created to On Hold'] = projects_with_holds_df.apply(lambda row:\
                                                                            row['Project Detail Hold Date On Hold'].date()\
                                                                            - row['Project Detail Created Date'].date(),1)\
                                                                          .dt.days

# COMMAND ----------

# data quality issues hold date before project created
projects_with_holds_df.loc[projects_with_holds_df['Days Created to On Hold'] < 0,
                    'Project Detail Hold Days on Hold'] = projects_with_holds_df['Project Detail Hold Days on Hold']-1

# COMMAND ----------

# for projects that ended on the same day we also need to subtract a day
projects_with_holds_df.loc[projects_with_holds_df['Project Detail Hold Date Off Hold'] 
                           == projects_with_holds_df['Project Detail Project Close Date Reporting'],
                        'Project Detail Hold Days on Hold'] = projects_with_holds_df['Project Detail Hold Days on Hold']-1

# COMMAND ----------

projects_with_holds_df['Days Created to On Hold'] = projects_with_holds_df['Days Created to On Hold'].replace(-1,0)\
                                                                                                     .astype(int)

# COMMAND ----------

projects_with_holds_df['Days Created to Off Hold'] = projects_with_holds_df['Days Created to On Hold'] \
                                                    + projects_with_holds_df['Project Detail Hold Days on Hold']\
                                                        .astype(int)

# we will have 10 bins
projects_with_holds_df['Day Rate per Bin'] = projects_with_holds_df['Project Length: Day Created to Closed']/10

# COMMAND ----------

projects_with_holds_df['Bin'] = (projects_with_holds_df['Days Created to On Hold']\
                                          /projects_with_holds_df['Day Rate per Bin']).apply(np.ceil)

# COMMAND ----------

projects_with_holds_df[projects_with_holds_df['Bin'].isnull()]

# COMMAND ----------

# 'float' object has no attribute 'apply', so 0.0/# = 0.0 which throws an error leading to a nan
# if we have a 0 then the hold occured on the same day as created, so that hold belongs in bin 1
projects_with_holds_df['Bin'] = projects_with_holds_df['Bin'].fillna(1).replace(0,1)

# COMMAND ----------

projects_with_holds_df['Hold Till Bin'] = (projects_with_holds_df['Project Detail Hold Days on Hold']\
                                          /projects_with_holds_df['Day Rate per Bin']).apply(np.floor)

# COMMAND ----------

projects_with_holds_df['Bin'].unique()

# COMMAND ----------

# 'float' object has no attribute 'apply', so 0.0/# = 0.0 which throws an error leading to a nan
# if we have a 0 then the hold occured on the same day as created, so that hold belongs in bin 1
projects_with_holds_df['Hold Till Bin'] = projects_with_holds_df['Hold Till Bin'].fillna(1).replace(np.inf,1).astype(int)

# COMMAND ----------

projects_with_holds_df[projects_with_holds_df['Hold Till Bin']== np.inf]

# COMMAND ----------

projects_with_holds_df['Number of Hold Days in Bin'] = ((projects_with_holds_df['Day Rate per Bin']
                                                        * projects_with_holds_df['Bin'])
                                                        - projects_with_holds_df['Days Created to On Hold']).round(2)

# COMMAND ----------

test = projects_with_holds_df[projects_with_holds_df['Project Detail Record ID']=="a9T1L00000000SgUAI"]
test

# COMMAND ----------

duplicate_row_df = pd.DataFrame()

for index, row in projects_with_holds_df.iterrows():
    # if we only have one shift in bins then we just the final row difference
    if row['Hold Till Bin'] != 1:
        # range cuts one off the end
        ranged_to_num = 1
        if row['Hold Till Bin'] == 10:
            ranged_to_num = row['Hold Till Bin']
        else: 
            ranged_to_num = row['Hold Till Bin']+1
            
        for x in range(1, ranged_to_num):
            temp_df = row.to_frame().T
            temp_df['Bin'] = row['Bin']+x
            temp_df['Number of Hold Days in Bin'] = row['Day Rate per Bin']
            temp_df['Hold Till Bin'] = np.nan
            if duplicate_row_df.empty:
                duplicate_row_df = temp_df
            else:
                duplicate_row_df = pd.concat([duplicate_row_df,temp_df])
            
    final_row = row.to_frame().T
    if (row['Bin']+row['Hold Till Bin'] >= 10):
        # hold will need to look at the max of bin 9 then subra
        final_row['Number of Hold Days in Bin'] = row['Days Created to Off Hold']\
                                                    - (row['Day Rate per Bin']*(row['Bin']+row['Hold Till Bin']-1))
                                                    
        final_row['Bin'] = row['Bin']+row['Hold Till Bin']
    elif row['Days Created to Off Hold'] > (row['Day Rate per Bin']*(row['Bin']+row['Hold Till Bin'])):
        final_row['Number of Hold Days in Bin'] = row['Days Created to Off Hold']\
                                                    - (row['Day Rate per Bin']*(row['Bin']+row['Hold Till Bin']))
    else: 
        final_row['Number of Hold Days in Bin'] = (row['Day Rate per Bin']*(row['Bin']+row['Hold Till Bin']))\
                                                    - row['Days Created to Off Hold']
        
        final_row['Bin'] = row['Bin']+row['Hold Till Bin']+1
        
    final_row['Hold Till Bin'] = np.nan
    duplicate_row_df = pd.concat([duplicate_row_df,final_row])


    

# COMMAND ----------

# in rare case bin 10 is last bin will have 2 of the same rows ex. "a9T1L0000008OSfUAM"
duplicate_row_df = duplicate_row_df[duplicate_row_df['Number of Hold Days in Bin']>0].infer_objects()\
                                    .reset_index(drop=True)
duplicate_row_df['Number of Hold Days in Bin'] = duplicate_row_df['Number of Hold Days in Bin'].astype(float).round(2)
duplicate_row_df = duplicate_row_df.drop_duplicates()

# COMMAND ----------

duplicate_row_df

# COMMAND ----------

projects_hold_bin_break_down_df = pd.concat([duplicate_row_df,projects_with_holds_df])\
                                    .drop("Hold Till Bin",1)

# COMMAND ----------



# COMMAND ----------

group_by_col_ls = list(projects_hold_bin_break_down_df.columns)
group_by_col_ls.remove("Number of Hold Days in Bin")
group_by_col_ls.remove("Bin")

# COMMAND ----------

projects_hold_bin_break_down_df = pd.pivot_table(projects_hold_bin_break_down_df, index = group_by_col_ls, columns=['Bin'],
                                                 values=["Number of Hold Days in Bin"], aggfunc = np.sum, fill_value=0)\
                                                 .reset_index()
projects_hold_bin_break_down_df.columns = group_by_col_ls + ['Hold Days in 0-10% of Project',
                                                             'Hold Days in 10-20% of Project',
                                                             'Hold Days in 20-30% of Project',
                                                             'Hold Days in 30-40% of Project',
                                                             'Hold Days in 40-50% of Project',
                                                             'Hold Days in 50-60% of Project',
                                                             'Hold Days in 60-70% of Project',
                                                             'Hold Days in 70-80% of Project',
                                                             'Hold Days in 80-90% of Project',
                                                             'Hold Days in 90-100% of Project']

# COMMAND ----------

projects_hold_bin_break_down_df

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ## Comparing Projects with Hold and without Holds
# MAGIC ### We need to check if we have to do a process of over/undersampling depending on the ratio of Hold v.s. non Hold projects. If ratio is very high like 10 to 1 then we will sample.

# COMMAND ----------

# only needed values
key_hold_distribution_col_ls = ['Project Detail Record ID', 'Hold Days in 0-10% of Project', 
                                'Hold Days in 10-20% of Project','Hold Days in 20-30% of Project', 
                                'Hold Days in 30-40% of Project', 'Hold Days in 40-50% of Project', 
                                'Hold Days in 50-60% of Project', 'Hold Days in 60-70% of Project',
                                'Hold Days in 70-80% of Project', 'Hold Days in 80-90% of Project',
                                'Hold Days in 90-100% of Project']

ratio_comparision_df = pd.merge(project_base_df, projects_hold_bin_break_down_df[key_hold_distribution_col_ls], 
                                how = 'left', on = 'Project Detail Record ID')

# COMMAND ----------

ratio_comparision_df

# COMMAND ----------

ratio_comparision_df['Hold Occured'] = ratio_comparision_df['Project Detail Total Days On Hold'].apply(lambda value: 1 
                                                                  if value > 0 
                                                                  else 0,1)
ratio_comparision_df.groupby('Hold Occured')['Project Detail Record ID'].nunique()

# COMMAND ----------

projects_with_holds_num = ratio_comparision_df.groupby('Hold Occured')['Project Detail Record ID'].nunique()[1]
projects_with_holds_num

# COMMAND ----------

# almost
sns.countplot(x='Hold Occured', data=ratio_comparision_df)

# COMMAND ----------

all_projects_with_holds_df = ratio_comparision_df[ratio_comparision_df['Hold Occured']==1]

# COMMAND ----------

# we are gonna set a 3 to 1 ratio of non hold to holds
samples_without_holds_df = ratio_comparision_df[ratio_comparision_df['Hold Occured']==0].dropna(thresh=100)\
                                                .sample(n=projects_with_holds_num*3, random_state=1)
samples_without_holds_df

# COMMAND ----------

projects_feature_base_df = pd.concat([all_projects_with_holds_df,samples_without_holds_df]).drop('Hold Occured',1)

# COMMAND ----------

for s in ['Hold Days in 0-10% of Project','Hold Days in 10-20% of Project','Hold Days in 20-30% of Project', 
          'Hold Days in 30-40% of Project', 'Hold Days in 40-50% of Project','Hold Days in 50-60% of Project', 
          'Hold Days in 60-70% of Project', 'Hold Days in 70-80% of Project', 'Hold Days in 80-90% of Project',
          'Hold Days in 90-100% of Project']:
    projects_feature_base_df[s] = projects_feature_base_df[s].fillna(0)

# COMMAND ----------

# Dropping Cols

# COMMAND ----------

columns_for_inspection_dic = flagging_unhelpful_columns(project_base_df)

# COMMAND ----------

list(columns_for_inspection_dic.keys())

# COMMAND ----------

projects_feature_base_df = projects_feature_base_df.drop(['Project Detail Project Close Date Reporting', 
                                                          'Project Detail Owner Email', 'Project Detail Number of Holds',
                                                          'Project Detail Total Days On Hold', 
                                                          'Project Length: Day Created to Closed',
                                                          'Project Detail Number of Holds', 
                                                          'Project Detail Total Days On Hold',
                                                          'Project Detail Remaining Hours/Day',
                                                          'Project Detail Days from Today to Planned Project Close']
                                                         + list(columns_for_inspection_dic.keys()),1)

# COMMAND ----------

projects_feature_base_df['Project Detail Currency'].dtype == "object"

# COMMAND ----------

for col in projects_feature_base_df.columns:

    if (projects_feature_base_df[col].dtype == "object") and (projects_feature_base_df[col].nunique()<= 10): 
        print(col)
        projects_feature_base_df = pd.get_dummies(data=projects_feature_base_df, columns=[col], drop_first=True)

# COMMAND ----------

projects_feature_base_df = engineering_date_features(projects_feature_base_df)

# COMMAND ----------

projects_feature_base_df

# COMMAND ----------

projects_feature_base_df.to_csv(Configuration_File.PROJECT_START_DATE_DELAY_FEATURE_BASE, index = None, header=True)

# COMMAND ----------


