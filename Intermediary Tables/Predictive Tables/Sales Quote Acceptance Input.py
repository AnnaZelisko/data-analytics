# Databricks notebook source
# MAGIC %md
# MAGIC # Sales Quote Acceptance Input Creation Table
# MAGIC ### Creating the general input table for the sales quote acceptance model.
# MAGIC 
# MAGIC <a href="#Accessing_Data">Accessing Data</a><br>
# MAGIC <a href="#Transforming_Columns">Transforming Columns</a><br>
# MAGIC <a href="#Grouping_Quote_Lines_by_Quote">Grouping Quote Lines by Quote</a><br>
# MAGIC <a href="#Splitting_Table">Splitting Table</a><br>

# COMMAND ----------

import sys
import os

# loads main directory
dir_to_data = os.getcwd().split('data-analytics',1)[1].count("\\")

string = '..\\' * dir_to_data
    
sys.path.insert(0,string)

# loads additional directories
from ipynb.fs.full.System_Path_Set_Up import system_link 
system_link(string)

# COMMAND ----------

import pandas as pd
import numpy as np

from ipynb.fs.full.Getting_Data import GET_from_Excel, GET_from_Centra_SQL
from ipynb.fs.full.Cleaning_Tables import flagging_unhelpful_columns, flagging_id_columns,flagging_columns_high_correlation
from ipynb.fs.full.Column_Transformations import transforming_tf_to_binary_columns, engineering_date_features

import Configuration_File

# COMMAND ----------

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Accessing_Data'></a>
# MAGIC 
# MAGIC ## Accessing Data
# MAGIC ### Data from New bookings will drive the model as the report informs us of which quotes have been accepted and rejected.
# MAGIC ### Also immediatly joining with Quote Base table.

# COMMAND ----------

New_Booking = GET_from_Excel(Configuration_File.NEWBOOKINGSANNALINK, "xlsx", 
                             {"sheet_name":Configuration_File.NEWBOOKINGSANNASHEET})\
                .rename(columns={'Stage':"Target"})
New_Booking['Join ID'] =  New_Booking['Quote ID'] + New_Booking['Quote Number']

# COMMAND ----------

# created the joing 
New_Booking = New_Booking.drop(['Quote ID', 'Quote Number'],1)

# COMMAND ----------

Quote = GET_from_Excel(Configuration_File.QUOTE_BASE_BUSINESS, "csv")

# COMMAND ----------

Quote['Quote SF ID'] = Quote['Quote Record ID'].str.slice(stop=15)
Quote['Join ID'] =  Quote['Quote SF ID'] + Quote['Quote Number']

# COMMAND ----------

record_type_df = GET_from_Centra_SQL(Configuration_File.RECORDTYPE, ['ID', 'Name']).rename(columns={'Name':"Record Type"})

# COMMAND ----------

Quote = pd.merge(Quote, record_type_df, how='left', left_on='Quote Record Type', right_on='ID')\
          .drop(['ID', 'Quote Record Type', 'Quote Projected Delivery Date Count'],1)

# COMMAND ----------

for col in Quote.columns:
    if ("Date" in col or "Expir" in col or "Modstamp" in col) and (Quote[col].dtype != "datetime64[ns]")\
        and (col != "Days To Expiration"):
            print(col)
            Quote[col] = pd.to_datetime(Quote[col], errors='ignore')

# COMMAND ----------

Quote.columns

# COMMAND ----------

# MAGIC %md
# MAGIC ## Joining Quote and New Booking

# COMMAND ----------

will_bias_model_ls = ['Quote Issued Date', 'Quote Accepted Date', 'Quote Days Since Quote Issued', 'Quote Win Reasons', 
                      'Quotes History Details Quote Approved Date', 'Quotes History Details Quote Approval Pending Date',
                      'Quotes History Details Quote Recalled Date', 'Quotes History Details Quote Approval Rejected Date', 
                      'Quote Close Date', 'Quote Status', 'Quote SF ID', 'Join ID']

# COMMAND ----------

Quote_Features = pd.merge(Quote, New_Booking, how='left', on='Join ID').dropna(subset=['Target'])\
                   .drop(will_bias_model_ls,1)

# COMMAND ----------

'Quote Expires On' in Quote.columns

# COMMAND ----------



# COMMAND ----------

# drop rows that are 80% empty
# Quote_Features = Quote_Features.loc[:, Quote_Features.isnull().mean() < .8]

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Transforming_Columns'></a>
# MAGIC 
# MAGIC ## Transforming Columns
# MAGIC ### Columns need to be transformed to prepare for 

# COMMAND ----------

ids_in_quote_features_ls = flagging_id_columns(Quote_Features)
ids_in_quote_features_ls.remove('Quote Record ID')

# COMMAND ----------

ids_in_quote_features_ls

# COMMAND ----------

Quote_Features = Quote_Features.drop(ids_in_quote_features_ls
                                     +['Quote SAP_Cust_Ref_ID', 'Quote SAP SO ID','Quote Number', 'Quote Bill To Name', 
                                       'Quote Ship To Name'],1)

# COMMAND ----------

Quote_Features = transforming_tf_to_binary_columns(Quote_Features)

# COMMAND ----------

#Most Quote have the same billing and ship address, so we can drop one or the other
Quote_Features['Quote Bill and Ship To Postal Code Are The Same'].sum()\
/Quote_Features['Quote Bill and Ship To Postal Code Are The Same'].count()

# COMMAND ----------

Quote_Features['Quote Bill and Ship To City Are The Same'].sum()\
/Quote_Features['Quote Bill and Ship To City Are The Same'].count()

# COMMAND ----------

Quote_Features['Quote Bill and Ship To State Are The Same'].sum()\
/Quote_Features['Quote Bill and Ship To State Are The Same'].count()


# COMMAND ----------

# Based on group bys Bill To is more filled in than ship too
Quote_Features['Quote Bill and Ship To Country Are The Same'].sum()\
/Quote_Features['Quote Bill and Ship To Country Are The Same'].count()

# COMMAND ----------

Quote_Features = Quote_Features.drop(['Quote Ship To Country','Quote Ship To State','Quote Ship To City',
                                      'Quote Ship To Postal Code'],1)

# COMMAND ----------

Quote_Features

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Grouping_Quote_Lines_by_Quote'></a>
# MAGIC 
# MAGIC ## Grouping Quote Lines by Quote
# MAGIC ### Quote Lines store a lot of valuable information about the quote however there can be many quote lines for one quote. As a result we need to transform the features to incorporate them into our model.

# COMMAND ----------

# Quote Line Product Name rather than Quote Line Product Code as code is more generic. 
# Ex. Name has C0-Model Cert. Model Cert.while Code is NAMODEL

#"Quote Line Product Code" is a shortened of "Quote Line Service" and "Quote Line Subservice"
# Product Name is the 
# 'Quote Line Currency' we have Quote Currency
# 'Quote Line Service Delivery Location' we have Quote SDL
QuoteLine = GET_from_Excel(Configuration_File.QUOTELINE_BASE_BUSINESS,"csv")\
            .drop(['Quote Line Product Name', 'Quote Line Service',
                   'Quote Line Subservice', 'Quote Line Currency',
                   'Quote Line Service Delivery Location',
                   'Quote Line Customer Product Description','Quote Line Standards',  'Quote Line Primary Contact Email',
                   'Quote Line Scope Check Completed By', 'Quote Line Project Status', 'Quote Line Provisions',
                   'Quote Line SAP_Service_Tracking_ID', 'Quote Line SAP_SO_Item', 'Quote Line Close Date',
                  'Quote Line Approval Time', 'Quote Line Scope Check Completed Date'],1)

# COMMAND ----------

for col in QuoteLine.columns:
    if ("Date" in col or "Modstamp" in col) and (QuoteLine[col].dtype != "datetime64[ns]")\
        and (col != 'Quote Line Planned Start Date - Quote Create Date'):
            print(col)
            QuoteLine[col] = pd.to_datetime(QuoteLine[col], errors='ignore')

# COMMAND ----------

QuoteLine['Quote Line Planned Start Date'].unique()

# COMMAND ----------



# COMMAND ----------

QuoteLine[QuoteLine['Quote Line Quote']=="a0l1L00000Bra03QAB"]

# COMMAND ----------

ids_in_QL_ls = flagging_id_columns(QuoteLine)
ids_in_QL_ls.remove('Quote Line Quote')

# COMMAND ----------

QuoteLine = QuoteLine.drop(ids_in_QL_ls,1)

# COMMAND ----------

QuoteLine = transforming_tf_to_binary_columns(QuoteLine)

# COMMAND ----------

group_agg_dic = {'Quote Line Line Name':"count"}
strings_index_ls = ['Quote Line Quote']
quote_quoteline_pivot_df = QuoteLine[['Quote Line Quote']]

columns_to_traverse = list(QuoteLine.columns)
columns_to_traverse.remove('Quote Line Quote')
columns_to_traverse.remove('Quote Line Line Name')

fill_w_0_group_by_ls = []

for col in columns_to_traverse:
    if(QuoteLine[col].dtype == np.float64 or QuoteLine[col].dtype == np.int64):
        group_agg_dic[col] = [np.sum, np.mean]
        fill_w_0_group_by_ls.append(col +"_Sum")
        fill_w_0_group_by_ls.append(col +"_Mean")
    elif QuoteLine[col].dtype == "datetime64[ns]":
        group_agg_dic[col] = [np.min]
    else: 
        # Quote Line [col].dtype == np.object string 
        print(col)
        QuoteLine[col] = col+ " "+ QuoteLine[col]
        group_by = QuoteLine.groupby('Quote Line Quote')[col].count().reset_index()        
        counter = group_by[col].max()
        if counter==1:
            strings_index_ls.append(col)
        else: 
            temp_df = pd.pivot_table(QuoteLine, values='Quote Line Line Name', index='Quote Line Quote',\
                       columns=col, aggfunc="count", fill_value=0).reset_index()
            quote_quoteline_pivot_df = pd.merge(quote_quoteline_pivot_df, temp_df, how='left', on='Quote Line Quote')\
                                         .drop_duplicates()

# COMMAND ----------

strings_index_ls

# COMMAND ----------

fill_w_0_pivot_ls = list(quote_quoteline_pivot_df.columns)
fill_w_0_pivot_ls.remove('Quote Line Quote')

# COMMAND ----------

quote_quoteline_pivot_df['Quote Line Quote'].count()

# COMMAND ----------

quote_quoteline_pivot_df['Quote Line Quote'].nunique()

# COMMAND ----------

quote_quoteline_aggs_df = QuoteLine.groupby(strings_index_ls).agg(group_agg_dic).reset_index()

# COMMAND ----------

new_columns = ['{}_{}'.format(col, function.title())
               for col, function in quote_quoteline_aggs_df.columns]
new_columns.remove("Quote Line Quote_")
new_columns = ["Quote Line Quote"] + new_columns

# COMMAND ----------

quote_quoteline_aggs_df.columns = new_columns

# COMMAND ----------

quote_quoteline_aggs_df['Quote Line Quote'].nunique()

# COMMAND ----------

quote_quoteline_aggs_df['Quote Line Quote'].count()

# COMMAND ----------

all_quoteline_info_for_quote_df = pd.merge(quote_quoteline_pivot_df, quote_quoteline_aggs_df, 
                                           how='left', on='Quote Line Quote')

# COMMAND ----------

# droping joinging columns and we have Quote Line Currency pivoted into columns already 
quote_features_with_ql_df = pd.merge(Quote_Features, all_quoteline_info_for_quote_df, how='left', 
                                     left_on='Quote Record ID', right_on='Quote Line Quote').drop_duplicates()\
                              .drop(['Quote Line Quote'],1)

# COMMAND ----------

'Quote Expires On' in Quote_Features.columns

# COMMAND ----------

'Quote Expires On' in quote_features_with_ql_df.columns

# COMMAND ----------

for col in fill_w_0_group_by_ls+fill_w_0_pivot_ls:
    quote_features_with_ql_df[col] = quote_features_with_ql_df[col].fillna(0)

# COMMAND ----------

quote_features_with_ql_df

# COMMAND ----------

# MAGIC %md
# MAGIC ## Fill nans with most common values or means
# MAGIC ### Some of our column values have nulls of nans we want to fill those with the mean/min or most common occuring iteam in our column. In the future this data quality should be improved over all

# COMMAND ----------

#Filling in Quotes History Details Quote Earliest\Latest Expiration
quote_features_with_ql_df.loc[quote_features_with_ql_df['Quotes History Details Quote Earliest Expiration'].isnull(),
                  'Quotes History Details Quote Earliest Expiration'] = quote_features_with_ql_df['Quote Expires On']
quote_features_with_ql_df.loc[quote_features_with_ql_df['Quotes History Details Quote Latest Expiration'].isnull(),
                  'Quotes History Details Quote Latest Expiration'] = quote_features_with_ql_df['Quote Expires On']

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Quote Expiration Date_Amin'].isnull(),
                  'Quote Line Quote Expiration Date_Amin'] = quote_features_with_ql_df['Quote Expires On']

# COMMAND ----------

# input created date for quoteline as quote created date
quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Created Date_Amin'].isnull(),
                  'Quote Line Created Date_Amin'] = quote_features_with_ql_df['Quote Created Date']

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Last Modified Date_Amin'].isnull(),
                  'Quote Line Last Modified Date_Amin'] = quote_features_with_ql_df['Quote Last Modified Date']

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line System Modstamp_Amin'].isnull(),
                  'Quote Line System Modstamp_Amin'] = quote_features_with_ql_df['Quote System Modstamp']

# COMMAND ----------

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Quote Created Date_Amin'].isnull(),
                  'Quote Line Quote Created Date_Amin'] = quote_features_with_ql_df['Quote Created Date']

# COMMAND ----------

for x in quote_features_with_ql_df.columns:
    if quote_features_with_ql_df[x].isnull().sum() >0:
        if "Quotes History Details" in x and quote_features_with_ql_df[x].dtype == np.float64:
            quote_features_with_ql_df[x] = quote_features_with_ql_df[x].fillna(0)
        elif "Quotes Stage" in x:
            quote_features_with_ql_df = quote_features_with_ql_df.drop(x,1)
        else:  
            print(x)
            print(quote_features_with_ql_df[x].isnull().sum())
            print(quote_features_with_ql_df[x].dtype)

# COMMAND ----------

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Pricing Date_Amin'].isnull()]

# COMMAND ----------

quote_features_with_ql_df.loc[quote_features_with_ql_df['Quote Line Pricing Date_Amin'].notnull()]

# COMMAND ----------

hard_to_fill = ['Quote PO Number', 'Quote Account Billing Postal Code',
                'Quote Submitted Date','Quote Bill To Postal Code', 
                'Quote Line Planned Start Date_Amin',  'Quote Line Pricing Date_Amin', 
                'Quote Line Cust. Desired Date_Amin','Quote Line Projected Delivery Date_Amin']

fill_w_most_common = ['Quote Bill To City', 'Quote Bill To Country', 'Quote Bill To State', 
                      'Quote Preferred Communication Language', 'Quote Owner Title', 'Quote Customer GBU', 'Quote Region',
                      'Quote Primary Contact Status', 'Quote Ship To Customer Status', 'Quote Master Contract Status',
                      'Quote Region Country', 'Quote Region City', 'Quote Primary Service Delivery Location Country',
                      'Quote Primary Service Delivery Location City']

fill_w_0 = ['Quote Resource Total', 'Quote Approval Deposit Percentage', 'Quote IFE needed', 'Quote Version #']
fill_w_1 = ['Quote Line Line Name_Count']
fill_w_mean = ['Quote Corporate Conversion Rate', 'Quote Exchange Rate', 'Quote Weeks to Certify Total',
               'Quote Corporate Exchange Rate', 'Days To Expiration', 'Days Outstanding',
               'Quote Average Additional Discounts']

# COMMAND ----------

quote_features_with_ql_df = quote_features_with_ql_df.drop(hard_to_fill,1)

# COMMAND ----------

for col in fill_w_0:
    quote_features_with_ql_df[col] = quote_features_with_ql_df[col].fillna(0)

# COMMAND ----------

for col in fill_w_1:
    quote_features_with_ql_df[col] = quote_features_with_ql_df[col].fillna(1)

# COMMAND ----------

for col in fill_w_mean:
    print(col)
    col_mean_num = quote_features_with_ql_df[col].mean()
    print(col_mean_num)
    quote_features_with_ql_df[col] = quote_features_with_ql_df[col].fillna(col_mean_num)

# COMMAND ----------

# filling in based on above lists
for col in fill_w_most_common:
    print(col)
    col_mean_str = quote_features_with_ql_df[col].mode()[0]
    print(col_mean_str)
    quote_features_with_ql_df[col] = quote_features_with_ql_df[col].fillna(col_mean_str)

# COMMAND ----------

for col in quote_features_with_ql_df.columns:
    if (quote_features_with_ql_df[col].dtype == "object") and (quote_features_with_ql_df[col].nunique()<= 10)\
        and (col != "Target"): 
            quote_features_with_ql_df = pd.get_dummies(data=quote_features_with_ql_df, columns=[col], drop_first=True)

# COMMAND ----------

quote_features_with_ql_df = engineering_date_features(quote_features_with_ql_df)

# COMMAND ----------

# MAGIC %md
# MAGIC <a id='Splitting_Table'></a>
# MAGIC 
# MAGIC ## Splitting table
# MAGIC ### Currently data set contains information on which we can train/validate/test a model and data for which we want predictions. This is defined by the Target column brought in with New Bookings. Quotes with this column filled in will be used to train our model.

# COMMAND ----------

model_training_df = quote_features_with_ql_df[quote_features_with_ql_df['Target'].isin(["6. Closed Lost", 
                                                                  "5. Ready for Operations", 
                                                                  "5. Ready for Operations - Exclude from Forecast"])]

# COMMAND ----------

model_training_df['Target'] =  model_training_df['Target'].replace({"6. Closed Lost":1, 
                                                                    "5. Ready for Operations":0, 
                                                                    "5. Ready for Operations - Exclude from Forecast":0})

# COMMAND ----------

future_prediction_df = quote_features_with_ql_df[~quote_features_with_ql_df['Target'].isin(["6. Closed Lost", 
                                                                  "5. Ready for Operations", 
                                                                  "5. Ready for Operations - Exclude from Forecast"])]\
                       .drop('Target', 1)

# COMMAND ----------

# getting rid of unhelpful columns
unhelpful_col_train_dic = flagging_unhelpful_columns(model_training_df)

# COMMAND ----------

# getting rid of unhelpful columns
unhelpful_col_future_dic = flagging_unhelpful_columns(future_prediction_df)

# COMMAND ----------

columns_high_correlation_ls = flagging_columns_high_correlation(model_training_df, 'Target')

# COMMAND ----------

model_training_df

# COMMAND ----------

model_training_df = model_training_df.drop(columns_high_correlation_ls + list(unhelpful_col_train_dic.keys())
                                            + list(unhelpful_col_future_dic.keys())+['Quote Record ID'],1)

# COMMAND ----------

model_training_df

# COMMAND ----------

future_prediction_df = future_prediction_df.drop(list(unhelpful_col_train_dic.keys()) + columns_high_correlation_ls
                                                 + list(unhelpful_col_future_dic.keys()),1)

# COMMAND ----------

future_prediction_df

# COMMAND ----------

model_training_df.to_csv(Configuration_File.SALES_QUOTE_ACCPTENCE_FEATURE_BASE, index = None, header=True)

# COMMAND ----------

future_prediction_df.to_csv(Configuration_File.SALES_QUOTE_ACCPTENCE_FOR_PREDITION, index = None, header=True)

# COMMAND ----------


